package io.gitlab.gitlabcidkjava.pipeline

import io.gitlab.gitlabcidkjava.model.pipeline.Pipeline
import io.gitlab.gitlabcidkjava.model.pipeline.job.Job
import io.gitlab.gitlabcidkjava.model.pipeline.job.JobWhen
import io.gitlab.gitlabcidkjava.model.pipeline.job.Variable
import io.gitlab.gitlabcidkjava.model.pipeline.job.artifacts.Artifacts
import io.gitlab.gitlabcidkjava.model.pipeline.job.artifacts.Report
import io.gitlab.gitlabcidkjava.model.pipeline.job.artifacts.ReportString
import io.gitlab.gitlabcidkjava.model.pipeline.job.cache.Cache
import io.gitlab.gitlabcidkjava.model.pipeline.job.cache.CacheDefinition
import io.gitlab.gitlabcidkjava.model.pipeline.job.image.Image
import io.gitlab.gitlabcidkjava.model.pipeline.job.needs.Need
import io.gitlab.gitlabcidkjava.model.pipeline.job.needs.NeedsList
import io.gitlab.gitlabcidkjava.model.pipeline.job.rules.Rule
import io.gitlab.gitlabcidkjava.model.pipeline.job.trigger.Strategy
import io.gitlab.gitlabcidkjava.model.pipeline.job.trigger.Trigger
import io.gitlab.gitlabcidkjava.renderer.PipelineRenderer
import java.io.File

const val STAGE_NAME = "stageless"

fun prepareBuildPageJob(): Job {
    val image = Image.builder()
        .name("node:10")
        .build()

    val cache = Cache.builder()
        .caches(
            mutableListOf(
                CacheDefinition.builder()
                    .paths(mutableListOf("node_modules/"))
                    .build()
            )
        )
        .build()

    val script = mutableListOf(
        "npm install gitbook-cli -g",
        "gitbook fetch 3.2.3",
        "gitbook install",
        "gitbook build . public",
        "cp -r target/site/apidocs public/apidocs",
        "echo \"JOB_ID=\$CI_JOB_ID\" >> \"job_id.env\""
    )

    val artifacts = Artifacts.builder()
        .expireIn("1 week")
        .paths(
            mutableListOf(
                "public"
            )
        )
        .reports(
            mutableListOf<Report>(
                ReportString("dotenv", "job_id.env")
            )
        )
        .build()

    val needs = NeedsList(
        mutableListOf(
            Need.builder()
                .job("site")
                .artifacts(true)
                .build()
        )
    )

    return Job.builder()
        .id("build-page")
        .image(image)
        .stage(STAGE_NAME)
        .cache(cache)
        .script(script)
        .artifacts(artifacts)
        .needs(needs)
        .build()
}

fun preparePublishPageJob(buildPageJob: Job): Job {
    val needs = NeedsList(
        mutableListOf(
            Need.builder()
                .job(buildPageJob.id)
                .artifacts(true)
                .build()
        )
    )

    val trigger = Trigger.builder()
        .project("gitlab-cidk/gitlab-cidk.gitlab.io")
        .strategy(Strategy.DEPEND)
        .build()

    val variables = mutableListOf(
        Variable.builder()
            .key("PROJECT_ID")
            .value("\$CI_PROJECT_ID")
            .build(),
        Variable.builder()
            .key("JOB_ID")
            .value("\$JOB_ID")
            .build()
    )

    val rules = mutableListOf(
        Rule.builder()
            .ifExpression("\$CI_COMMIT_REF_NAME == \$CI_DEFAULT_BRANCH")
            .build(),
        Rule.builder()
            .ifExpression("\$CI_COMMIT_REF_NAME != \$CI_DEFAULT_BRANCH")
            .`when`(JobWhen.MANUAL)
            .allowFailure(true)
            .build()
    )

    return Job.builder()
        .id("publish-page")
        .stage(STAGE_NAME)
        .needs(needs)
        .trigger(trigger)
        .variables(variables)
        .rules(rules)
        .build()
}

fun prepareSiteJob(): Job {
    val artifacts = Artifacts.builder()
        .paths(
            mutableListOf(
                "target/site/**/*"
            )
        )
        .build()

    return Job.builder()
        .id("site")
        .image(
            Image.builder()
                .name("eclipse-temurin:8u382-b05-jdk-focal")
                .build()
        )
        .stage(STAGE_NAME)
        .script(
            mutableListOf(
                "/bin/sh ./mvnw site"
            )
        )
        .artifacts(artifacts)
        .build()
}

fun prepareVerifyJob(): Job {
    val artifacts = Artifacts.builder()
        .paths(
            mutableListOf(
                "target/surefire-reports/*.xml"
            )
        )
        .reports(
            mutableListOf<Report>(
                ReportString("junit", "target/surefire-reports/*.xml")
            )
        )
        .build()

    return Job.builder()
        .id("verify")
        .image(
            Image.builder()
                .name("eclipse-temurin:8u382-b05-jdk-focal")
                .build()
        )
        .stage(STAGE_NAME)
        .script(
            mutableListOf(
                "/bin/sh ./mvnw verify"
            )
        )
        .artifacts(artifacts)
        .build()
}

class PipelineGenerator {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val buildPageJob = prepareBuildPageJob()
            val publishPageJob = preparePublishPageJob(buildPageJob)

            val pipeline = Pipeline.builder()
                .stages(mutableListOf(STAGE_NAME))
                .jobs(
                    mutableListOf(
                        buildPageJob,
                        publishPageJob,
                        prepareVerifyJob(),
                        prepareSiteJob()
                    )
                )
                .build()

            val renderer = PipelineRenderer()

            File("target/.gitlab-ci.yml").bufferedWriter().use { writer ->
                println(renderer.toWriter(pipeline, writer))
            }
        }
    }
}
