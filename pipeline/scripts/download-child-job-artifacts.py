# This job finds a job in the child pipeline and downloads its artifacts.
# This is used to publish test results in the parent pipeline.

import argparse
import urllib.parse
import urllib.request
import json
import os
import zipfile
import io


def find_child_pipeline_id(args):
    api_url = f"projects/{args.project_id}/pipelines/{args.pipeline_id}/bridges"
    url = urllib.parse.urljoin(args.gitlab_api_url, api_url)
    headers = {"PRIVATE-TOKEN": args.access_token}
    request = urllib.request.Request(url, headers=headers, method="GET")
    response = urllib.request.urlopen(request)
    content = response.read().decode("utf-8")
    json_content = json.loads(content)
    for trigger_job in json_content:
        if trigger_job["name"] == args.trigger_job_name:
            return trigger_job["downstream_pipeline"]["id"]
    raise ValueError("Job not found")


def find_child_job_id(args, child_pipeline_id):
    api_url = f"projects/{args.project_id}/pipelines/{child_pipeline_id}/jobs"
    url = urllib.parse.urljoin(args.gitlab_api_url, api_url)
    headers = {"PRIVATE-TOKEN": args.access_token}
    request = urllib.request.Request(url, headers=headers, method="GET")
    response = urllib.request.urlopen(request)
    content = response.read().decode("utf-8")
    json_content = json.loads(content)
    for child_job in json_content:
        if child_job["name"] == args.child_job_name:
            return child_job["id"]
    raise ValueError("Job not found")


def download_artifacts(args, child_job_id):
    api_url = f"projects/{args.project_id}/jobs/{child_job_id}/artifacts"
    url = urllib.parse.urljoin(args.gitlab_api_url, api_url)
    headers = {"PRIVATE-TOKEN": args.access_token}
    request = urllib.request.Request(url, headers=headers, method="GET")
    response = urllib.request.urlopen(request)
    content = response.read()
    os.makedirs(args.output_dir, exist_ok=True)
    with zipfile.ZipFile(io.BytesIO(content)) as archive:
        archive.extractall(args.output_dir)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--gitlab-api-url", default="https://gitlab.com/api/v4/")
    parser.add_argument("--project-id", required=True)
    parser.add_argument("--pipeline-id", required=True)
    parser.add_argument("--access-token", required=True)
    parser.add_argument("--trigger-job-name", required=True)
    parser.add_argument("--child-job-name", required=True)
    parser.add_argument("--output-dir", default="artifacts")
    args = parser.parse_args()

    child_pipeline_id = find_child_pipeline_id(args)
    child_job_id = find_child_job_id(args, child_pipeline_id)
    download_artifacts(args, child_job_id)


if __name__ == "__main__":
    main()
