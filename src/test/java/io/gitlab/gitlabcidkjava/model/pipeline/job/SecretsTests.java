package io.gitlab.gitlabcidkjava.model.pipeline.job;

import io.gitlab.gitlabcidkjava.model.pipeline.Pipeline;
import io.gitlab.gitlabcidkjava.model.pipeline.job.idtokens.IdToken;
import io.gitlab.gitlabcidkjava.model.pipeline.job.idtokens.IdTokens;
import io.gitlab.gitlabcidkjava.model.pipeline.job.secrets.AzureKeyVault;
import io.gitlab.gitlabcidkjava.model.pipeline.job.secrets.Secret;
import io.gitlab.gitlabcidkjava.model.pipeline.job.secrets.Vault;
import io.gitlab.gitlabcidkjava.model.pipeline.test.Utils;
import java.io.IOException;
import java.util.Arrays;
import org.junit.Test;

public class SecretsTests {
  @Test
  public void vaultTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job")
                .secrets(Arrays.asList(
                    Secret.builder()
                        .id("DATABASE_PASSWORD")
                        .vault(Vault.builder()
                            .engineName("kv-v2")
                            .enginePath("ops")
                            .path("production/db")
                            .field("password")
                            .build())
                        .build()
                ))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/secrets/vault.yml", pipeline);
  }

  @Test
  public void azureKeyVaultTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job")
                .secrets(Arrays.asList(
                    Secret.builder()
                        .id("DATABASE_PASSWORD")
                        .azureKeyVault(AzureKeyVault.builder()
                            .name("test")
                            .version("test")
                            .build())
                        .build()
                ))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/secrets/azure_key_vault.yml", pipeline);
  }

  @Test
  public void fileTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job")
                .secrets(Arrays.asList(
                    Secret.builder()
                        .id("DATABASE_PASSWORD")
                        .vault(Vault.builder()
                            .engineName("kv-v2")
                            .enginePath("ops")
                            .path("production/db")
                            .field("password")
                            .build())
                        .file(false)
                        .build()
                ))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/secrets/file.yml", pipeline);
  }

  @Test
  public void tokenTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job")
                .idTokens(IdTokens.builder()
                    .idTokens(Arrays.asList(
                        new IdToken("AWS_TOKEN", Arrays.asList("https://aws.example.com")),
                        new IdToken("VAULT_TOKEN", Arrays.asList("https://vault.example.com"))
                    ))
                    .build())
                .secrets(Arrays.asList(
                    Secret.builder()
                        .id("DB_PASSWORD")
                        .vault(Vault.builder()
                            .engineName("kv-v2")
                            .enginePath("ops")
                            .path("production/db")
                            .field("password")
                            .build())
                        .token("$VAULT_TOKEN")
                        .build()
                ))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/secrets/token.yml", pipeline);
  }


}
