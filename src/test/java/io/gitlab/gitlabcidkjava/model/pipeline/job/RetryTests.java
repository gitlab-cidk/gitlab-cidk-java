package io.gitlab.gitlabcidkjava.model.pipeline.job;

import io.gitlab.gitlabcidkjava.model.pipeline.Pipeline;
import io.gitlab.gitlabcidkjava.model.pipeline.job.retry.Retry;
import io.gitlab.gitlabcidkjava.model.pipeline.job.retry.RetryWhen;
import io.gitlab.gitlabcidkjava.model.pipeline.test.Utils;
import java.io.IOException;
import java.util.Arrays;
import org.junit.Test;

public class RetryTests {
  @Test
  public void basicTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("test")
                .script(Arrays.asList("rspec"))
                .retry(Retry.builder()
                    .max(2)
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/retry/basic.yml", pipeline);
  }

  @Test
  public void whenTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("test")
                .script(Arrays.asList("rspec"))
                .retry(Retry.builder()
                    .max(2)
                    .when(Arrays.asList(
                        RetryWhen.RUNNER_SYSTEM_FAILURE,
                        RetryWhen.STUCK_OR_TIMEOUT_FAILURE
                    ))
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/retry/when.yml", pipeline);
  }
}
