package io.gitlab.gitlabcidkjava.model.pipeline.job;

import io.gitlab.gitlabcidkjava.model.pipeline.Pipeline;
import io.gitlab.gitlabcidkjava.model.pipeline.job.cache.Cache;
import io.gitlab.gitlabcidkjava.model.pipeline.job.cache.CacheDefinition;
import io.gitlab.gitlabcidkjava.model.pipeline.job.cache.CacheDefinitionKeyFilesPrefix;
import io.gitlab.gitlabcidkjava.model.pipeline.job.cache.CacheDefinitionKeyString;
import io.gitlab.gitlabcidkjava.model.pipeline.job.cache.CacheWhen;
import io.gitlab.gitlabcidkjava.model.pipeline.job.cache.Policy;
import io.gitlab.gitlabcidkjava.model.pipeline.test.Utils;

import java.io.IOException;
import java.util.Arrays;
import org.junit.Test;

public class CacheTests {
  @Test
  public void pathsTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("rspec")
                .script(Arrays.asList("echo \"This job uses a cache.\""))
                .cache(new Cache(Arrays.asList(
                    CacheDefinition.builder()
                        .key(new CacheDefinitionKeyString("binaries-cache"))
                        .paths(Arrays.asList("binaries/*.apk", ".config"))
                        .build()
                )))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/cache/paths.yml", pipeline);
  }

  @Test
  public void keyTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("cache-job")
                .script(Arrays.asList("echo \"This job uses a cache.\""))
                .cache(new Cache(Arrays.asList(
                    CacheDefinition.builder()
                        .key(new CacheDefinitionKeyString("binaries-cache-$CI_COMMIT_REF_SLUG"))
                        .paths(Arrays.asList("binaries/"))
                        .build()
                )))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/cache/key.yml", pipeline);
  }

  @Test
  public void filesTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("cache-job")
                .script(Arrays.asList("echo \"This job uses a cache.\""))
                .cache(new Cache(Arrays.asList(
                    CacheDefinition.builder()
                        .key(CacheDefinitionKeyFilesPrefix.builder()
                            .files(Arrays.asList("Gemfile.lock", "package.json"))
                            .build())
                        .paths(Arrays.asList("vendor/ruby", "node_modules"))
                        .build()
                )))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/cache/files.yml", pipeline);
  }

  @Test
  public void prefixTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("rspec")
                .script(Arrays.asList("echo \"This rspec job uses a cache.\""))
                .cache(new Cache(Arrays.asList(
                    CacheDefinition.builder()
                        .key(CacheDefinitionKeyFilesPrefix.builder()
                            .files(Arrays.asList("Gemfile.lock"))
                            .prefix("$CI_JOB_NAME")
                            .build())
                        .paths(Arrays.asList("vendor/ruby"))
                        .build()
                )))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/cache/prefix.yml", pipeline);
  }

  @Test
  public void untrackedTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("rspec")
                .script(Arrays.asList("test"))
                .cache(new Cache(Arrays.asList(
                    CacheDefinition.builder()
                        .untracked(true)
                        .build()
                )))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/cache/untracked.yml", pipeline);
  }

  @Test
  public void untrackedPathsTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("rspec")
                .script(Arrays.asList("test"))
                .cache(new Cache(Arrays.asList(
                    CacheDefinition.builder()
                        .paths(Arrays.asList("binaries/"))
                        .untracked(true)
                        .build()
                )))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/cache/untracked_paths.yml", pipeline);
  }

  @Test
  public void unprotectTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("rspec")
                .script(Arrays.asList("test"))
                .cache(new Cache(Arrays.asList(
                    CacheDefinition.builder()
                        .unprotect(true)
                        .build()
                )))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/cache/unprotect.yml", pipeline);
  }

  @Test
  public void whenTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("rspec")
                .script(Arrays.asList("rspec"))
                .cache(new Cache(Arrays.asList(
                    CacheDefinition.builder()
                        .paths(Arrays.asList("rspec/"))
                        .when(CacheWhen.ALWAYS)
                        .build()
                )))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/cache/when.yml", pipeline);
  }

  @Test
  public void policyTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("prepare-dependencies-job")
                .stage("build")
                .script(Arrays.asList(
                    "echo \"This job only downloads dependencies and builds the cache.\"",
                    "echo \"Downloading dependencies...\""
                ))
                .cache(new Cache(Arrays.asList(
                    CacheDefinition.builder()
                        .key(new CacheDefinitionKeyString("gems"))
                        .paths(Arrays.asList("vendor/bundle"))
                        .policy(Policy.PUSH)
                        .build()
                )))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/cache/policy.yml", pipeline);
  }

  @Test
  public void fallbackKeysTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("rspec")
                .script(Arrays.asList("rspec"))
                .cache(new Cache(Arrays.asList(
                    CacheDefinition.builder()
                        .key(new CacheDefinitionKeyString("gems-$CI_COMMIT_REF_SLUG"))
                        .paths(Arrays.asList("rspec/"))
                        .when(CacheWhen.ALWAYS)
                        .fallbackKeys(Arrays.asList("gems"))
                        .build()
                )))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/cache/fallback_keys.yml", pipeline);
  }
}
