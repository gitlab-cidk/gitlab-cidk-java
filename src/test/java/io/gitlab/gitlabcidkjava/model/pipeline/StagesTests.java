package io.gitlab.gitlabcidkjava.model.pipeline;

import java.io.IOException;
import java.util.Arrays;
import org.junit.Test;

import io.gitlab.gitlabcidkjava.model.pipeline.test.Utils;

public class StagesTests {
  @Test
  public void stagesTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .stages(Arrays.asList("build", "test", "deploy"))
        .build();
    Utils.assertYamlEquals("stages.yml", pipeline);
  }
}
