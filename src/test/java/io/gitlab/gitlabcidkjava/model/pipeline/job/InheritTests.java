package io.gitlab.gitlabcidkjava.model.pipeline.job;

import java.io.IOException;
import java.util.Arrays;

import org.junit.Test;

import io.gitlab.gitlabcidkjava.model.pipeline.Pipeline;
import io.gitlab.gitlabcidkjava.model.pipeline.PipelineDefault;
import io.gitlab.gitlabcidkjava.model.pipeline.job.image.Image;
import io.gitlab.gitlabcidkjava.model.pipeline.job.inherit.Inherit;
import io.gitlab.gitlabcidkjava.model.pipeline.job.inherit.InheritDefaultBoolean;
import io.gitlab.gitlabcidkjava.model.pipeline.job.inherit.InheritDefaultList;
import io.gitlab.gitlabcidkjava.model.pipeline.job.inherit.InheritVariablesBoolean;
import io.gitlab.gitlabcidkjava.model.pipeline.job.inherit.InheritVariablesList;
import io.gitlab.gitlabcidkjava.model.pipeline.job.retry.Retry;
import io.gitlab.gitlabcidkjava.model.pipeline.test.Utils;

public class InheritTests {
  @Test
  public void defaultTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .pipelineDefault(PipelineDefault.builder()
            .retry(Retry.builder()
                .max(2)
                .build())
            .image(Image.builder()
                .name("ruby:3.0")
                .build())
            .interruptible(true)
            .build())
        .jobs(Arrays.asList(
            Job.builder()
                .id("job1")
                .script(Arrays.asList("echo \"This job does not inherit any default keywords.\""))
                .inherit(Inherit.builder()
                    .inheritDefault(new InheritDefaultBoolean(false))
                    .build())
                .build(),
            Job.builder()
                .id("job2")
                .script(Arrays.asList(
                    "echo \"This job inherits only the two listed default keywords. It does not inherit 'interruptible'.\""))
                .inherit(Inherit.builder()
                    .inheritDefault(new InheritDefaultList(Arrays.asList("retry", "image")))
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/inherit/default.yml", pipeline);
  }

  @Test
  public void variablesTest() throws IOException {
    Variable variable1 = Variable.builder()
        .key("VARIABLE1")
        .value("This is variable 1")
        .build();
    Variable variable2 = Variable.builder()
        .key("VARIABLE2")
        .value("This is variable 2")
        .build();
    Variable variable3 = Variable.builder()
        .key("VARIABLE3")
        .value("This is variable 3")
        .build();
    Pipeline pipeline = Pipeline.builder()
        .variables(Arrays.asList(variable1, variable2, variable3))
        .jobs(Arrays.asList(
            Job.builder()
                .id("job1")
                .script(Arrays.asList("echo \"This job does not inherit any global variables.\""))
                .inherit(Inherit.builder()
                    .inheritVariables(new InheritVariablesBoolean(false))
                    .build())
                .build(),
            Job.builder()
                .id("job2")
                .script(Arrays.asList(
                    "echo \"This job inherits only the two listed global variables. It does not inherit 'VARIABLE3'.\""))
                .inherit(Inherit.builder()
                    .inheritVariables(new InheritVariablesList(Arrays.asList("VARIABLE1",
                        "VARIABLE2")))
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/inherit/variables.yml", pipeline);
  }
}
