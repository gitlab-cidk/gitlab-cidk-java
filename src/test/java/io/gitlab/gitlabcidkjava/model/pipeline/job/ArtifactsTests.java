package io.gitlab.gitlabcidkjava.model.pipeline.job;

import io.gitlab.gitlabcidkjava.model.pipeline.Pipeline;
import io.gitlab.gitlabcidkjava.model.pipeline.job.artifacts.Artifacts;
import io.gitlab.gitlabcidkjava.model.pipeline.job.artifacts.ReportString;
import io.gitlab.gitlabcidkjava.model.pipeline.test.Utils;
import io.gitlab.gitlabcidkjava.model.pipeline.job.artifacts.ArtifactWhen;
import java.io.IOException;
import java.util.Arrays;
import org.junit.Test;

public class ArtifactsTests {
  @Test
  public void pathsTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job")
                .artifacts(Artifacts.builder()
                    .paths(Arrays.asList("binaries/", ".config"))
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/artifacts/paths.yml", pipeline);
  }

  @Test
  public void excludeTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job")
                .artifacts(Artifacts.builder()
                    .paths(Arrays.asList("binaries/"))
                    .exclude(Arrays.asList("binaries/**/*.o"))
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/artifacts/exclude.yml", pipeline);
  }

  @Test
  public void expireInTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job")
                .artifacts(Artifacts.builder()
                    .expireIn("1 week")
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/artifacts/expire_in.yml", pipeline);
  }

  @Test
  public void exposeAsTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("test")
                .script(Arrays.asList("echo 'test' > file.txt"))
                .artifacts(Artifacts.builder()
                    .exposeAs("artifact 1")
                    .paths(Arrays.asList("file.txt"))
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/artifacts/expose_as.yml", pipeline);
  }

  @Test
  public void nameTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job")
                .artifacts(Artifacts.builder()
                    .name("job1-artifacts-file")
                    .paths(Arrays.asList("binaries/"))
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/artifacts/name.yml", pipeline);
  }

  @Test
  public void publicTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job")
                .artifacts(Artifacts.builder()
                    .isPublic(false)
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/artifacts/public.yml", pipeline);
  }

  @Test
  public void reportsTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("rspec")
                .stage("test")
                .script(Arrays.asList("bundle install",
                    "rspec --format RspecJunitFormatter --out rspec.xml"))
                .artifacts(Artifacts.builder()
                    .reports(Arrays.asList(
                        new ReportString("junit", "rspec.xml")
                    ))
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/artifacts/reports.yml", pipeline);
  }

  @Test
  public void untrackedTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job")
                .artifacts(Artifacts.builder()
                    .untracked(true)
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/artifacts/untracked.yml", pipeline);
  }

  @Test
  public void whenTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job")
                .artifacts(Artifacts.builder()
                    .when(ArtifactWhen.ON_FAILURE)
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/artifacts/when.yml", pipeline);
  }
}
