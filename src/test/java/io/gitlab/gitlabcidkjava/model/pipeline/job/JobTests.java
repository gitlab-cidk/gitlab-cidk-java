package io.gitlab.gitlabcidkjava.model.pipeline.job;

import io.gitlab.gitlabcidkjava.model.pipeline.Pipeline;
import io.gitlab.gitlabcidkjava.model.pipeline.PipelineInclude;
import io.gitlab.gitlabcidkjava.model.pipeline.job.artifacts.Artifacts;
import io.gitlab.gitlabcidkjava.model.pipeline.job.environment.Environment;
import io.gitlab.gitlabcidkjava.model.pipeline.job.idtokens.IdToken;
import io.gitlab.gitlabcidkjava.model.pipeline.job.idtokens.IdTokens;
import io.gitlab.gitlabcidkjava.model.pipeline.test.Utils;
import java.io.IOException;
import java.util.Arrays;
import org.junit.Test;

public class JobTests {
  @Test
  public void afterScriptTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job")
                .script(Arrays.asList("echo \"An example script section.\""))
                .afterScript(
                    Arrays.asList(
                        "echo \"Execute this command after the `script` section completes.\""
                    )
                )
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/after_script.yml", pipeline);
  }

  @Test
  public void beforeScriptTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job")
                .script(Arrays.asList(
                    "echo \"This command executes after the job's 'before_script' commands.\""))
                .beforeScript(
                    Arrays.asList(
                        "echo \"Execute this command before any 'script:' commands.\""
                    )
                )
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/before_script.yml", pipeline);
  }

  @Test
  public void coverageTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job1")
                .script(Arrays.asList("rspec"))
                .coverage("/Code coverage: \\d+\\.\\d+/")
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/coverage.yml", pipeline);
  }

  @Test
  public void dastConfigurationTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .stages(Arrays.asList(
            "build",
            "dast"
        ))
        .include(Arrays.asList(
            PipelineInclude.builder()
                .template("DAST.gitlab-ci.yml")
                .build()
        ))
        .jobs(Arrays.asList(
            Job.builder()
                .id("dast")
                .dastConfiguration(DastConfiguration.builder()
                    .siteProfile("Example Co")
                    .scannerProfile("Quick Passive Test")
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/dast_configuration.yml", pipeline);
  }

  @Test
  public void dependenciesTest() throws IOException {
    Job buildOsxJob = Job.builder()
        .id("build osx")
        .stage("build")
        .script(Arrays.asList("make build:osx"))
        .artifacts(Artifacts.builder()
            .paths(Arrays.asList("binaries/"))
            .build())
        .build();

    Job buildLinuxJob = Job.builder()
        .id("build linux")
        .stage("build")
        .script(Arrays.asList("make build:linux"))
        .artifacts(Artifacts.builder()
            .paths(Arrays.asList("binaries/"))
            .build())
        .build();

    Job testOsxJob = Job.builder()
        .id("test osx")
        .stage("test")
        .script(Arrays.asList("make test:osx"))
        .dependencies(Arrays.asList(buildOsxJob))
        .build();

    Job testLinuxJob = Job.builder()
        .id("test linux")
        .stage("test")
        .script(Arrays.asList("make test:linux"))
        .dependencies(Arrays.asList(buildLinuxJob))
        .build();

    Job deployJob = Job.builder()
        .id("deploy")
        .stage("deploy")
        .script(Arrays.asList("make deploy"))
        .environment(Environment.builder()
            .name("production")
            .build())
        .build();

    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(buildOsxJob, buildLinuxJob, testOsxJob, testLinuxJob,
            deployJob))
        .build();

    Utils.assertYamlEquals("job/dependencies.yml", pipeline);
  }

  @Test
  public void extendsTest() throws IOException {
    Job testsJob = Job.builder()
        .id(".tests")
        .script(Arrays.asList("rake test"))
        .stage("test")
        .build();

    Job rspecJob = Job.builder()
        .id("rspec")
        .jobExtends(new JobExtends(Arrays.asList(testsJob)))
        .script(Arrays.asList("rake rspec"))
        .build();

    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(testsJob, rspecJob))
        .build();
    Utils.assertYamlEquals("job/extends.yml", pipeline);
  }

  @Test
  public void hooksTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job1")
                .script(Arrays.asList("echo 'hello job1 script'"))
                .hooks(Hooks.builder()
                    .preGetSourcesScript(Arrays.asList(
                        "echo 'hello job1 pre_get_sources_script'"))
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/hooks.yml", pipeline);
  }

  @Test
  public void idTokensTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job_with_id_tokens")
                .script(Arrays.asList(
                    "command_to_authenticate_with_gitlab $ID_TOKEN_1",
                    "command_to_authenticate_with_aws $ID_TOKEN_2"
                ))
                .idTokens(new IdTokens(Arrays.asList(
                    IdToken.builder()
                        .id("ID_TOKEN_1")
                        .audiences(Arrays.asList("https://gitlab.com"))
                        .build(),
                    IdToken.builder()
                        .id("ID_TOKEN_2")
                        .audiences(Arrays.asList("https://gcp.com",
                            "https://aws.com"))
                        .build()
                )))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/id_tokens.yml", pipeline);
  }

  @Test
  public void interruptibleTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("step-1")
                .stage("stage1")
                .script(Arrays.asList("echo \"Can be canceled.\""))
                .interruptible(true)
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/interruptible.yml", pipeline);
  }

  @Test
  public void resourceGroupTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("deploy-to-production")
                .script(Arrays.asList("deploy"))
                .resourceGroup("production")
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/resource_group.yml", pipeline);
  }

  @Test
  public void scriptTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job1")
                .script(Arrays.asList("bundle exec rspec"))
                .build(),
            Job.builder()
                .id("job2")
                .script(Arrays.asList(
                    "uname -a",
                    "bundle exec rspec"
                ))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/script.yml", pipeline);
  }

  @Test
  public void stageTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .stages(Arrays.asList(
            "build",
            "test",
            "deploy"
        ))
        .jobs(Arrays.asList(
            Job.builder()
                .id("job1")
                .stage("build")
                .script(Arrays.asList("echo \"This job compiles code.\""))
                .build(),
            Job.builder()
                .id("job2")
                .stage("test")
                .script(Arrays.asList(
                    "echo \"This job tests the compiled code. It runs when the build stage completes.\""))
                .build(),
            Job.builder()
                .id("job3")
                .script(Arrays.asList("echo \"This job also runs in the test stage\""))
                .build(),
            Job.builder()
                .id("job4")
                .stage("deploy")
                .script(Arrays.asList(
                    "echo \"This job deploys the code. It runs when the test stage completes.\""))
                .environment(Environment.builder()
                    .name("production")
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/stage.yml", pipeline);
  }

  @Test
  public void tagsTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job")
                .tags(Arrays.asList("ruby", "postgres"))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/tags.yml", pipeline);
  }

  @Test
  public void timeoutTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("build")
                .script(Arrays.asList("build.sh"))
                .timeout("3 hours 30 minutes")
                .build(),
            Job.builder()
                .id("test")
                .script(Arrays.asList("rspec"))
                .timeout("3h 30m")
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/timeout.yml", pipeline);
  }

  @Test
  public void whenTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .stages(Arrays.asList(
            "build",
            "cleanup_build",
            "test",
            "deploy",
            "cleanup"
        ))
        .jobs(Arrays.asList(
            Job.builder()
                .id("build_job")
                .stage("build")
                .script(Arrays.asList("make build"))
                .build(),
            Job.builder()
                .id("cleanup_build_job")
                .stage("cleanup_build")
                .script(Arrays.asList("cleanup build when failed"))
                .when(JobWhen.ON_FAILURE)
                .build(),
            Job.builder()
                .id("test_job")
                .stage("test")
                .script(Arrays.asList("make test"))
                .build(),
            Job.builder()
                .id("deploy_job")
                .stage("deploy")
                .script(Arrays.asList("make deploy"))
                .when(JobWhen.MANUAL)
                .environment(Environment.builder()
                    .name("production")
                    .build())
                .build(),
            Job.builder()
                .id("cleanup_job")
                .stage("cleanup")
                .script(Arrays.asList("cleanup after jobs"))
                .when(JobWhen.ALWAYS)
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/when.yml", pipeline);
  }
}
