package io.gitlab.gitlabcidkjava.model.pipeline.job;

import io.gitlab.gitlabcidkjava.model.pipeline.Pipeline;
import io.gitlab.gitlabcidkjava.model.pipeline.job.environment.Environment;
import io.gitlab.gitlabcidkjava.model.pipeline.job.parallel.ParallelMatrix;
import io.gitlab.gitlabcidkjava.model.pipeline.job.parallel.ParallelNumber;
import io.gitlab.gitlabcidkjava.model.pipeline.job.parallel.Variable;
import io.gitlab.gitlabcidkjava.model.pipeline.test.Utils;
import java.io.IOException;
import java.util.Arrays;
import org.junit.Test;

public class ParallelTests {
  @Test
  public void numberTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("test")
                .script(Arrays.asList("rspec"))
                .parallel(new ParallelNumber(5))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/parallel/number.yml", pipeline);
  }

  @Test
  public void matrixTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("deploystacks")
                .stage("deploy")
                .script(Arrays.asList("bin/deploy"))
                .parallel(new ParallelMatrix(Arrays.asList(
                    Arrays.asList(new Variable("PROVIDER", Arrays.asList("aws")),
                        new Variable("STACK", Arrays.asList("monitoring", "app1", "app2"))),
                    Arrays.asList(new Variable("PROVIDER", Arrays.asList("ovh")),
                        new Variable("STACK", Arrays.asList("monitoring", "backup", "app"))),
                    Arrays.asList(new Variable("PROVIDER", Arrays.asList("gcp", "vultr")),
                        new Variable("STACK", Arrays.asList("data", "processing")))
                )))
                .environment(Environment.builder()
                    .name("$PROVIDER/$STACK")
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/parallel/matrix.yml", pipeline);
  }
}
