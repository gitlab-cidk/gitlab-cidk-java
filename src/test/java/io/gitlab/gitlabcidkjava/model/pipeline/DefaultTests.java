package io.gitlab.gitlabcidkjava.model.pipeline;

import io.gitlab.gitlabcidkjava.model.pipeline.job.Hooks;
import io.gitlab.gitlabcidkjava.model.pipeline.job.Service;
import io.gitlab.gitlabcidkjava.model.pipeline.job.artifacts.Artifacts;
import io.gitlab.gitlabcidkjava.model.pipeline.job.cache.Cache;
import io.gitlab.gitlabcidkjava.model.pipeline.job.cache.CacheDefinition;
import io.gitlab.gitlabcidkjava.model.pipeline.job.cache.CacheDefinitionKeyString;
import io.gitlab.gitlabcidkjava.model.pipeline.job.image.Image;
import io.gitlab.gitlabcidkjava.model.pipeline.job.retry.Retry;
import io.gitlab.gitlabcidkjava.model.pipeline.test.Utils;

import java.io.IOException;
import java.util.Arrays;
import org.junit.Test;

public class DefaultTests {
  @Test
  public void defaultAfterScriptTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .pipelineDefault(
            PipelineDefault.builder()
                .afterScript(
                    Arrays.asList(
                        "echo \"Execute this command after the `script` section completes.\""
                    )
                )
                .build()
        )
        .build();
    Utils.assertYamlEquals("default/after_script.yml", pipeline);
  }

  @Test
  public void defaultArtifactsTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .pipelineDefault(
            PipelineDefault.builder()
                .artifacts(
                    Artifacts.builder()
                        .paths(Arrays.asList("binaries/", ".config"))
                        .build()
                )
                .build()
        )
        .build();
    Utils.assertYamlEquals("default/artifacts.yml", pipeline);
  }

  @Test
  public void defaultBeforeScriptTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .pipelineDefault(
            PipelineDefault.builder()
                .beforeScript(
                    Arrays.asList(
                        "echo \"Execute this command before the `script` section starts.\""
                    )
                )
                .build()
        )
        .build();
    Utils.assertYamlEquals("default/before_script.yml", pipeline);
  }

  @Test
  public void defaultCacheTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .pipelineDefault(
            PipelineDefault.builder()
                .cache(new Cache(Arrays.asList(
                    CacheDefinition.builder()
                        .key(new CacheDefinitionKeyString("binaries-cache"))
                        .paths(Arrays.asList("binaries/*.apk", ".config"))
                        .build()
                )))
                .build()
        )
        .build();
    Utils.assertYamlEquals("default/cache.yml", pipeline);
  }

  @Test
  public void defaultHooksTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .pipelineDefault(
            PipelineDefault.builder()
                .hooks(
                    Hooks.builder()
                        .preGetSourcesScript(
                            Arrays.asList("echo 'hello job1 pre_get_sources_script'"))
                        .build()
                )
                .build()
        )
        .build();
    Utils.assertYamlEquals("default/hooks.yml", pipeline);
  }

  @Test
  public void defaultImageTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .pipelineDefault(
            PipelineDefault.builder()
                .image(
                    Image.builder()
                        .name("ruby:3.0")
                        .build()
                )
                .build()
        )
        .build();
    Utils.assertYamlEquals("default/image.yml", pipeline);
  }

  @Test
  public void defaultInterruptibleTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .pipelineDefault(
            PipelineDefault.builder()
                .interruptible(true)
                .build()
        )
        .build();
    Utils.assertYamlEquals("default/interruptible.yml", pipeline);
  }

  @Test
  public void defaultRetryTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .pipelineDefault(
            PipelineDefault.builder()
                .retry(Retry.builder()
                    .max(2)
                    .build())
                .build()
        )
        .build();
    Utils.assertYamlEquals("default/retry.yml", pipeline);
  }

  @Test
  public void defaultServicesTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .pipelineDefault(
            PipelineDefault.builder()
                .services(Arrays.asList(
                    Service.builder()
                        .name("my-postgres:11.7")
                        .alias("db-postgres")
                        .entrypoint(Arrays.asList("/usr/local/bin/db-postgres"))
                        .command(Arrays.asList("start"))
                        .build()
                ))
                .build()
        )
        .build();
    Utils.assertYamlEquals("default/services.yml", pipeline);
  }

  @Test
  public void defaultTagsTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .pipelineDefault(
            PipelineDefault.builder()
                .tags(Arrays.asList("ruby", "postgres"))
                .build()
        )
        .build();
    Utils.assertYamlEquals("default/tags.yml", pipeline);
  }

  @Test
  public void defaultTimeoutTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .pipelineDefault(
            PipelineDefault.builder()
                .timeout("3 hours 30 minutes")
                .build()
        )
        .build();
    Utils.assertYamlEquals("default/timeout.yml", pipeline);
  }
}
