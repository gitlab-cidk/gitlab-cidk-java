package io.gitlab.gitlabcidkjava.model.pipeline.job;

import io.gitlab.gitlabcidkjava.model.pipeline.Pipeline;
import io.gitlab.gitlabcidkjava.model.pipeline.job.environment.Environment;
import io.gitlab.gitlabcidkjava.model.pipeline.test.Utils;
import java.io.IOException;
import java.util.Arrays;
import org.junit.Test;

public class Variables {
  @Test
  public void exampleTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .variables(Arrays.asList(
            Variable.builder()
                .key("DEPLOY_SITE")
                .value("https://example.com/")
                .build()
        ))
        .jobs(Arrays.asList(
            Job.builder()
                .id("deploy_job")
                .stage("deploy")
                .script(Arrays.asList("deploy-script --url $DEPLOY_SITE --path \"/\""))
                .environment(Environment.builder()
                    .name("production")
                    .build())
                .build(),
            Job.builder()
                .id("deploy_review_job")
                .stage("deploy")
                .variables(Arrays.asList(
                    Variable.builder()
                        .key("REVIEW_PATH")
                        .value("/review")
                        .build()
                ))
                .script(Arrays.asList(
                    "deploy-review-script --url $DEPLOY_SITE --path $REVIEW_PATH"))
                .environment(Environment.builder()
                    .name("production")
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/variables/example.yml", pipeline);
  }

  @Test
  public void descriptionTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .variables(Arrays.asList(
            Variable.builder()
                .key("DEPLOY_NOTE")
                .description("The deployment note. Explain the reason for this deployment.")
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/variables/description.yml", pipeline);
  }

  @Test
  public void valueTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .variables(Arrays.asList(
            Variable.builder()
                .key("DEPLOY_ENVIRONMENT")
                .value("staging")
                .description(
                    "The deployment target. Change this variable to 'canary' or 'production' if needed.")
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/variables/value.yml", pipeline);
  }

  @Test
  public void optionsTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .variables(Arrays.asList(
            Variable.builder()
                .key("DEPLOY_ENVIRONMENT")
                .value("staging")
                .options(Arrays.asList("production", "staging", "canary"))
                .description("The deployment target. Set to 'staging' by default.")
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/variables/options.yml", pipeline);
  }

  @Test
  public void expandTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .variables(Arrays.asList(
            Variable.builder()
                .key("VAR1")
                .value("value1")
                .build(),
            Variable.builder()
                .key("VAR2")
                .value("value2 $VAR1")
                .build(),
            Variable.builder()
                .key("VAR3")
                .value("value3 $VAR1")
                .expand(false)
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/variables/expand.yml", pipeline);
  }
}
