package io.gitlab.gitlabcidkjava.model.pipeline.job;

import io.gitlab.gitlabcidkjava.model.pipeline.Pipeline;
import io.gitlab.gitlabcidkjava.model.pipeline.job.allowfailure.AllowFailureBoolean;
import io.gitlab.gitlabcidkjava.model.pipeline.job.allowfailure.AllowFailureExitCodes;
import io.gitlab.gitlabcidkjava.model.pipeline.test.Utils;

import java.io.IOException;
import java.util.Arrays;
import org.junit.Test;

public class AllowFailureTests {
  @Test
  public void basicTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job2")
                .script(Arrays.asList("execute_script_2"))
                .allowFailure(new AllowFailureBoolean(true))
                .stage("test")
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/allow_failure/basic.yml", pipeline);
  }

  @Test
  public void exitCodesTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("test_job_2")
                .script(Arrays.asList(
                    "echo \"Run a script that results in exit code 137. This job is allowed to fail.\"",
                    "exit 137"))
                .allowFailure(new AllowFailureExitCodes(Arrays.asList(137, 255)))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/allow_failure/exit_codes.yml", pipeline);
  }
}
