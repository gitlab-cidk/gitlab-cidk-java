package io.gitlab.gitlabcidkjava.model.pipeline.test;

import static org.junit.Assert.assertEquals;

import com.esotericsoftware.yamlbeans.YamlException;
import com.esotericsoftware.yamlbeans.YamlReader;

import io.gitlab.gitlabcidkjava.model.pipeline.Pipeline;
import io.gitlab.gitlabcidkjava.renderer.PipelineRenderer;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Scanner;

public class Utils {
  private static <T> String readExpectedDocumentFromResources(Class<T> clazz, String filename)
      throws IOException {
    try (InputStream is = clazz.getResourceAsStream(filename)) {
      Scanner scanner = new Scanner(is, "UTF-8");
      String result = scanner.useDelimiter("\\A").next();
      scanner.close();
      return result;
    }
  }

  private static Object parseYaml(String yaml) throws YamlException {
    YamlReader reader = new YamlReader(new StringReader(yaml));
    return reader.read();
  }

  public static void assertYamlEquals(String expectedFilename, Pipeline pipeline)
      throws IOException {
    PipelineRenderer renderer = new PipelineRenderer();
    Object document = Utils.parseYaml(renderer.toString(pipeline));
    Object expected = Utils.parseYaml(
        Utils.readExpectedDocumentFromResources(Utils.class, expectedFilename));
    assertEquals(expected, document);
  }
}
