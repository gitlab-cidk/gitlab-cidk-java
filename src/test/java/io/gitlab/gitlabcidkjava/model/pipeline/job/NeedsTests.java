package io.gitlab.gitlabcidkjava.model.pipeline.job;

import java.io.IOException;
import java.util.Arrays;

import org.junit.Test;

import io.gitlab.gitlabcidkjava.model.pipeline.Pipeline;
import io.gitlab.gitlabcidkjava.model.pipeline.job.environment.Environment;
import io.gitlab.gitlabcidkjava.model.pipeline.job.needs.Need;
import io.gitlab.gitlabcidkjava.model.pipeline.job.needs.Needs;
import io.gitlab.gitlabcidkjava.model.pipeline.job.needs.NeedsList;
import io.gitlab.gitlabcidkjava.model.pipeline.job.needs.NeedsPipeline;
import io.gitlab.gitlabcidkjava.model.pipeline.test.Utils;

public class NeedsTests {
  @Test
  public void emptyTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("lint")
                .stage("test")
                .script(Arrays.asList("echo \"Linting...\""))
                .needs(new NeedsList(Arrays.asList()))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/needs/empty.yml", pipeline);
  }


  @Test
  public void artifactsTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("test-job2")
                .stage("test")
                .needs(new NeedsList(Arrays.asList(
                    Need.builder()
                        .job("build_job2")
                        .artifacts(false)
                        .build()
                )))
                .build(),
            Job.builder()
                .id("test-job3")
                .needs(new NeedsList(Arrays.asList(
                    Need.builder()
                        .job("build_job1")
                        .artifacts(true)
                        .build(),
                    Need.builder()
                        .job("build_job2")
                        .build()
                )))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/needs/artifacts.yml", pipeline);
  }

  @Test
  public void projectTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("build_job")
                .stage("build")
                .script(Arrays.asList("ls -lhR"))
                .needs(new NeedsList(Arrays.asList(
                    Need.builder()
                        .project("namespace/group/project-name")
                        .job("build-1")
                        .ref("main")
                        .artifacts(true)
                        .build(),
                    Need.builder()
                        .project("namespace/group/project-name-2")
                        .job("build-2")
                        .ref("main")
                        .artifacts(true)
                        .build(),
                    Need.builder()
                        .project("$CI_PROJECT_PATH")
                        .job("$DEPENDENCY_JOB_NAME")
                        .ref("$ARTIFACTS_DOWNLOAD_REF")
                        .artifacts(true)
                        .build()
                )))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/needs/project.yml", pipeline);
  }

  @Test
  public void pipelineJobTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("use-artifact")
                .script(Arrays.asList("cat artifact.txt"))
                .needs(new NeedsList(Arrays.asList(
                    Need.builder()
                        .pipeline("$PARENT_PIPELINE_ID")
                        .job("create-artifact")
                        .build()
                )))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/needs/pipeline_job.yml", pipeline);
  }

  @Test
  public void optionalTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("review-job")
                .stage("deploy")
                .needs(new NeedsList(Arrays.asList(
                    Need.builder()
                        .job("test-job2")
                        .optional(true)
                        .build()
                )))
                .environment(Environment.builder()
                    .name("review")
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/needs/optional.yml", pipeline);
  }

  @Test
  public void pipelineTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("upstream_status")
                .stage("test")
                .needs(new NeedsPipeline("other/project"))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/needs/pipeline.yml", pipeline);
  }
}
