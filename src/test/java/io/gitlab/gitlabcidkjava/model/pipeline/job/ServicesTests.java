package io.gitlab.gitlabcidkjava.model.pipeline.job;

import io.gitlab.gitlabcidkjava.model.pipeline.Pipeline;
import io.gitlab.gitlabcidkjava.model.pipeline.PipelineDefault;
import io.gitlab.gitlabcidkjava.model.pipeline.job.image.Image;
import io.gitlab.gitlabcidkjava.model.pipeline.job.image.PullPolicy;
import io.gitlab.gitlabcidkjava.model.pipeline.test.Utils;
import java.io.IOException;
import java.util.Arrays;
import org.junit.Test;

public class ServicesTests {
  @Test
  public void exampleTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .pipelineDefault(
            PipelineDefault.builder()
                .image(Image.builder()
                    .name("ruby:2.6")
                    .entrypoint(Arrays.asList("/bin/bash"))
                    .build())
                .services(Arrays.asList(
                    Service.builder()
                        .name("my-postgres:11.7")
                        .alias("db-postgres")
                        .entrypoint(Arrays.asList("/usr/local/bin/db-postgres"))
                        .command(Arrays.asList("start"))
                        .build()
                ))
                .beforeScript(Arrays.asList(
                    "bundle install"
                ))
                .build()
        )
        .jobs(Arrays.asList(
            Job.builder()
                .id("test")
                .script(Arrays.asList(
                    "bundle exec rake spec"
                ))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/services/example.yml", pipeline);
  }

  @Test
  public void pullPolicyTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job1")
                .script(Arrays.asList(
                    "echo \"A single pull policy.\""
                ))
                .services(Arrays.asList(
                    Service.builder()
                        .name("postgres:11.6")
                        .pullPolicies(Arrays.asList(
                            PullPolicy.IF_NOT_PRESENT
                        ))
                        .build()
                ))
                .build(),
            Job.builder()
                .id("job2")
                .script(Arrays.asList(
                    "echo \"Multiple pull policies.\""
                ))
                .services(Arrays.asList(
                    Service.builder()
                        .name("postgres:11.6")
                        .pullPolicies(Arrays.asList(
                            PullPolicy.ALWAYS,
                            PullPolicy.IF_NOT_PRESENT
                        ))
                        .build()
                ))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/services/pull_policy.yml", pipeline);
  }
}
