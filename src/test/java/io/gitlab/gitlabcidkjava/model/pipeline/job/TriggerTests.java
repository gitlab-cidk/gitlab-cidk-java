package io.gitlab.gitlabcidkjava.model.pipeline.job;

import io.gitlab.gitlabcidkjava.model.pipeline.Pipeline;
import io.gitlab.gitlabcidkjava.model.pipeline.job.trigger.Forward;
import io.gitlab.gitlabcidkjava.model.pipeline.job.trigger.Strategy;
import io.gitlab.gitlabcidkjava.model.pipeline.job.trigger.Trigger;
import io.gitlab.gitlabcidkjava.model.pipeline.test.Utils;
import java.io.IOException;
import java.util.Arrays;
import org.junit.Test;

public class TriggerTests {
  @Test
  public void includeTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("trigger-child-pipeline")
                .trigger(
                    Trigger.builder()
                        .include("path/to/child-pipeline.gitlab-ci.yml")
                        .build()
                )
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/trigger/include.yml", pipeline);
  }

  @Test
  public void projectTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("trigger-multi-project-pipeline")
                .trigger(
                    Trigger.builder()
                        .project("my-group/my-project")
                        .branch("development")
                        .build()
                )
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/trigger/project.yml", pipeline);
  }

  @Test
  public void strategyTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("trigger_job")
                .trigger(
                    Trigger.builder()
                        .include("path/to/child-pipeline.yml")
                        .strategy(Strategy.DEPEND)
                        .build()
                )
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/trigger/strategy.yml", pipeline);
  }

  @Test
  public void forwardTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .variables(Arrays.asList(
            Variable.builder()
                .key("VAR")
                .value("value")
                .build()
        ))
        .jobs(Arrays.asList(
            Job.builder()
                .id("child1")
                .trigger(
                    Trigger.builder()
                        .include(".child-pipeline.yml")
                        .build()
                )
                .build(),
            Job.builder()
                .id("child2")
                .trigger(
                    Trigger.builder()
                        .include(".child-pipeline.yml")
                        .forward(Forward.builder()
                            .pipelineVariables(true)
                            .build())
                        .build()
                )
                .build(),
            Job.builder()
                .id("child3")
                .trigger(
                    Trigger.builder()
                        .include(".child-pipeline.yml")
                        .forward(Forward.builder()
                            .yamlVariables(false)
                            .build())
                        .build()
                )
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/trigger/forward.yml", pipeline);
  }
}
