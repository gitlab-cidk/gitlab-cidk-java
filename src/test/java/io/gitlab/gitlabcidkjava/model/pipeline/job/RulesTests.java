package io.gitlab.gitlabcidkjava.model.pipeline.job;

import io.gitlab.gitlabcidkjava.model.pipeline.Pipeline;
import io.gitlab.gitlabcidkjava.model.pipeline.job.needs.Need;
import io.gitlab.gitlabcidkjava.model.pipeline.job.needs.NeedsList;
import io.gitlab.gitlabcidkjava.model.pipeline.job.rules.Changes;
import io.gitlab.gitlabcidkjava.model.pipeline.job.rules.Rule;
import io.gitlab.gitlabcidkjava.model.pipeline.test.Utils;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.junit.Test;

public class RulesTests {
  @Test
  public void ifTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job")
                .script(Arrays.asList("echo \"Hello, Rules!\""))
                .rules(Arrays.asList(
                    Rule.builder()
                        .ifExpression(
                            "$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^feature/ && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME != $CI_DEFAULT_BRANCH")
                        .when(JobWhen.NEVER)
                        .build(),
                    Rule.builder()
                        .ifExpression("$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^feature/")
                        .when(JobWhen.MANUAL)
                        .allowFailure(true)
                        .build(),
                    Rule.builder()
                        .ifExpression("$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME")
                        .build()
                ))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/rules/if.yml", pipeline);
  }

  @Test
  public void changesTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("docker build")
                .script(Arrays.asList("docker build -t my-image:$CI_COMMIT_REF_SLUG ."))
                .rules(Arrays.asList(
                    Rule.builder()
                        .ifExpression("$CI_PIPELINE_SOURCE == \"merge_request_event\"")
                        .changes(Changes.builder()
                            .paths(Arrays.asList("Dockerfile"))
                            .build())
                        .when(JobWhen.MANUAL)
                        .allowFailure(true)
                        .build()
                ))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/rules/changes_paths.yml", pipeline);
  }

  @Test
  public void changesCompareToTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("docker build")
                .script(Arrays.asList("docker build -t my-image:$CI_COMMIT_REF_SLUG ."))
                .rules(Arrays.asList(
                    Rule.builder()
                        .ifExpression("$CI_PIPELINE_SOURCE == \"merge_request_event\"")
                        .changes(Changes.builder()
                            .paths(Arrays.asList("Dockerfile"))
                            .compareTo("refs/heads/branch1")
                            .build())
                        .build()
                ))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/rules/changes_compare_to.yml", pipeline);
  }

  @Test
  public void existsTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job")
                .script(Arrays.asList("docker build -t my-image:$CI_COMMIT_REF_SLUG ."))
                .rules(Arrays.asList(
                    Rule.builder()
                        .exists(Arrays.asList("Dockerfile"))
                        .build()
                ))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/rules/exists.yml", pipeline);
  }

  @Test
  public void allowFailureTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job")
                .script(Arrays.asList("echo \"Hello, Rules!\""))
                .rules(Arrays.asList(
                    Rule.builder()
                        .ifExpression("$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $CI_DEFAULT_BRANCH")
                        .when(JobWhen.MANUAL)
                        .allowFailure(true)
                        .build()
                ))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/rules/allow_failure.yml", pipeline);
  }

  @Test
  public void needsTest() throws IOException {
    Job buildDevJob = Job.builder()
        .id("build-dev")
        .stage("build")
        .rules(Arrays.asList(
            Rule.builder()
                .ifExpression("$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH")
                .build()
        ))
        .script(Arrays.asList("echo \"Feature branch, so building dev version...\""))
        .build();

    Job buildProdJob = Job.builder()
        .id("build-prod")
        .stage("build")
        .rules(Arrays.asList(
            Rule.builder()
                .ifExpression("$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH")
                .build()
        ))
        .script(Arrays.asList("echo \"Default branch, so building prod version...\""))
        .build();

    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            buildDevJob,
            buildProdJob,
            Job.builder()
                .id("specs")
                .stage("test")
                .needs(new NeedsList(Arrays.asList(
                    Need.builder()
                        .job("build-dev")
                        .build()
                )))
                .script(Arrays.asList(
                    "echo \"Running dev specs by default, or prod specs when default branch...\""))
                .rules(Arrays.asList(
                    Rule.builder()
                        .ifExpression("$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH")
                        .needs(new NeedsList(Arrays.asList(
                            Need.builder()
                                .job("build-prod")
                                .build()
                        )))
                        .build(),
                    Rule.builder()
                        .when(JobWhen.ON_SUCCESS)
                        .build()
                ))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/rules/needs.yml", pipeline);
  }

  @Test
  public void variablesTest() throws IOException {
    final Map<String, String> variablesA = new HashMap<>();
    variablesA.put("DEPLOY_VARIABLE", "deploy-production");

    final Map<String, String> variablesB = new HashMap<>();
    variablesB.put("IS_A_FEATURE", "true");

    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job")
                .variables(Arrays.asList(
                    Variable.builder()
                        .key("DEPLOY_VARIABLE")
                        .value("default-deploy")
                        .build()
                ))
                .rules(Arrays.asList(
                    Rule.builder()
                        .ifExpression("$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH")
                        .variables(variablesA)
                        .build(),
                    Rule.builder()
                        .ifExpression("$CI_COMMIT_REF_NAME =~ /feature/")
                        .variables(variablesB)
                        .build()
                ))
                .script(Arrays.asList(
                    "echo \"Run script with $DEPLOY_VARIABLE as an argument\"",
                    "echo \"Run another script if $IS_A_FEATURE exists\""
                ))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/rules/variables.yml", pipeline);
  }
}
