package io.gitlab.gitlabcidkjava.model.pipeline;

import java.io.IOException;
import java.util.Arrays;
import org.junit.Test;

import io.gitlab.gitlabcidkjava.model.pipeline.test.Utils;

public class IncludeTests {
  @Test
  public void includeLocalTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .include(Arrays.asList(
            PipelineInclude.builder()
                .local("/templates/.gitlab-ci-template.yml")
                .build()
        ))
        .build();
    Utils.assertYamlEquals("include/local.yml", pipeline);
  }

  @Test
  public void includeProjectTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .include(Arrays.asList(
            PipelineInclude.builder()
                .project("my-group/my-project")
                .file(Arrays.asList("/templates/.builds.yml", "/templates/.tests.yml"))
                .ref("main")
                .build()
        ))
        .build();
    Utils.assertYamlEquals("include/project.yml", pipeline);
  }

  @Test
  public void includeRemoteProjectTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .include(Arrays.asList(
            PipelineInclude.builder()
                .remote("https://gitlab.com/example-project/-/raw/main/.gitlab-ci.yml")
                .build()
        ))
        .build();
    Utils.assertYamlEquals("include/remote.yml", pipeline);
  }

  @Test
  public void includeTemplateTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .include(Arrays.asList(
            PipelineInclude.builder()
                .template("Android-Fastlane.gitlab-ci.yml")
                .build(),
            PipelineInclude.builder()
                .template("Auto-DevOps.gitlab-ci.yml")
                .build()
        ))
        .build();
    Utils.assertYamlEquals("include/template.yml", pipeline);
  }
}
