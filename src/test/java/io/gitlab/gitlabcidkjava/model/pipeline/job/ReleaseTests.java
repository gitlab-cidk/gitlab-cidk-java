package io.gitlab.gitlabcidkjava.model.pipeline.job;

import io.gitlab.gitlabcidkjava.model.pipeline.Pipeline;
import io.gitlab.gitlabcidkjava.model.pipeline.job.image.Image;
import io.gitlab.gitlabcidkjava.model.pipeline.job.release.AssetLink;
import io.gitlab.gitlabcidkjava.model.pipeline.job.release.LinkType;
import io.gitlab.gitlabcidkjava.model.pipeline.job.release.Release;
import io.gitlab.gitlabcidkjava.model.pipeline.job.rules.Rule;
import io.gitlab.gitlabcidkjava.model.pipeline.test.Utils;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.Arrays;
import org.junit.Test;

public class ReleaseTests {
  @Test
  public void exampleTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("release_job")
                .stage("release")
                .image(Image.builder()
                    .name("registry.gitlab.com/gitlab-org/release-cli:latest")
                    .build())
                .rules(Arrays.asList(Rule.builder()
                    .ifExpression("$CI_COMMIT_TAG")
                    .build()))
                .script(Arrays.asList("echo \"Running the release job.\""))
                .release(Release.builder()
                    .tagName("$CI_COMMIT_TAG")
                    .name("Release $CI_COMMIT_TAG")
                    .description("Release created using the release-cli.")
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/release/example.yml", pipeline);
  }

  @Test
  public void tagNameTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job")
                .rules(Arrays.asList(Rule.builder()
                    .ifExpression("$CI_COMMIT_TAG")
                    .build()))
                .script(Arrays.asList("echo \"Running the release job for the new tag.\""))
                .release(Release.builder()
                    .tagName("$CI_COMMIT_TAG")
                    .description("Release description")
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/release/tag_name.yml", pipeline);
  }

  @Test
  public void tagMessageTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("release_job")
                .stage("release")
                .release(Release.builder()
                    .tagName("$CI_COMMIT_TAG")
                    .description("Release description")
                    .tagMessage("Annotated tag message")
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/release/tag_message.yml", pipeline);
  }

  @Test
  public void releaseNameTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("release_job")
                .stage("release")
                .release(Release.builder()
                    .tagName("$CI_COMMIT_TAG")
                    .name("Release $CI_COMMIT_TAG")
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/release/release_name.yml", pipeline);
  }

  @Test
  public void releasedAtTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("release_job")
                .stage("release")
                .release(Release.builder()
                    .tagName("$CI_COMMIT_TAG")
                    .releasedAt(ZonedDateTime.parse("2021-03-15T08:00:00Z"))
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/release/released_at.yml", pipeline);
  }

  @Test
  public void assetsLinksTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("release_job")
                .stage("release")
                .release(Release.builder()
                    .tagName("$CI_COMMIT_TAG")
                    .assetsLinks(Arrays.asList(
                        AssetLink.builder()
                            .name("asset1")
                            .url("https://example.com/assets/1")
                            .build(),
                        AssetLink.builder()
                            .name("asset2")
                            .url("https://example.com/assets/2")
                            .filePath("/pretty/url/1")
                            .linkType(LinkType.OTHER)
                            .build()
                    ))
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/release/assets_links.yml", pipeline);
  }
}
