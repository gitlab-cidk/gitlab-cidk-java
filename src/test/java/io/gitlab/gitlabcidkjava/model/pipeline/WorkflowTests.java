package io.gitlab.gitlabcidkjava.model.pipeline;

import io.gitlab.gitlabcidkjava.model.pipeline.job.Job;
import io.gitlab.gitlabcidkjava.model.pipeline.job.Variable;
import io.gitlab.gitlabcidkjava.model.pipeline.job.JobWhen;
import io.gitlab.gitlabcidkjava.model.pipeline.job.rules.Rule;
import io.gitlab.gitlabcidkjava.model.pipeline.test.Utils;
import io.gitlab.gitlabcidkjava.model.pipeline.workflow.Workflow;
import io.gitlab.gitlabcidkjava.model.pipeline.workflow.WorkflowRule;
import io.gitlab.gitlabcidkjava.model.pipeline.workflow.WorkflowWhen;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import org.junit.Test;

public class WorkflowTests {
  @Test
  public void stagesTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .workflow(Workflow.builder()
            .name("Pipeline for branch: $CI_COMMIT_BRANCH")
            .build())
        .build();
    Utils.assertYamlEquals("workflow/name.yml", pipeline);
  }

  @Test
  public void differentNamesTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .workflow(Workflow.builder()
            .name("$PROJECT1_PIPELINE_NAME")
            .rules(Arrays.asList(
                WorkflowRule.builder()
                    .ifExpression("$CI_PIPELINE_SOURCE == \"merge_request_event\"")
                    .variables(Collections.singletonMap("PROJECT1_PIPELINE_NAME",
                        "MR pipeline: $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME"))
                    .build(),
                WorkflowRule.builder()
                    .ifExpression("$CI_MERGE_REQUEST_LABELS =~ /pipeline:run-in-ruby3/")
                    .variables(
                        Collections.singletonMap("PROJECT1_PIPELINE_NAME", "Ruby 3 pipeline"))
                    .build())
            )
            .build())
        .build();
    Utils.assertYamlEquals("workflow/different_names.yml", pipeline);
  }

  @Test
  public void rulesTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .workflow(Workflow.builder()
            .rules(Arrays.asList(
                WorkflowRule.builder()
                    .ifExpression("$CI_COMMIT_TITLE =~ /-draft$/")
                    .when(WorkflowWhen.NEVER)
                    .build(),
                WorkflowRule.builder()
                    .ifExpression("$CI_PIPELINE_SOURCE == \"merge_request_event\"")
                    .build(),
                WorkflowRule.builder()
                    .ifExpression("$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH")
                    .build())
            )
            .build())
        .build();
    Utils.assertYamlEquals("workflow/rules.yml", pipeline);
  }

  @Test
  public void variablesTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .workflow(Workflow.builder()
            .rules(Arrays.asList(
                WorkflowRule.builder()
                    .ifExpression("$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH")
                    .variables(Collections.singletonMap("DEPLOY_VARIABLE", "deploy-production"))
                    .build(),
                WorkflowRule.builder()
                    .ifExpression("$CI_COMMIT_REF_NAME =~ /feature/")
                    .variables(Collections.singletonMap("IS_A_FEATURE", "true"))
                    .build(),
                WorkflowRule.builder()
                    .when(WorkflowWhen.ALWAYS)
                    .build()
            ))
            .build())
        .jobs(Arrays.asList(
            Job.builder()
                .id("job1")
                .variables(Arrays.asList(Variable.builder()
                    .key("DEPLOY_VARIABLE")
                    .value("job1-default-deploy")
                    .build()))
                .rules(Arrays.asList(
                    Rule.builder()
                        .ifExpression("$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH")
                        .variables(
                            Collections.singletonMap("DEPLOY_VARIABLE", "job1-deploy-production"))
                        .build(),
                    Rule.builder()
                        .when(JobWhen.ON_SUCCESS)
                        .build()
                ))
                .script(Arrays.asList(
                    "echo \"Run script with $DEPLOY_VARIABLE as an argument\"",
                    "echo \"Run another script if $IS_A_FEATURE exists\""
                ))
                .build(),
            Job.builder()
                .id("job2")
                .script(Arrays.asList(
                    "echo \"Run script with $DEPLOY_VARIABLE as an argument\"",
                    "echo \"Run another script if $IS_A_FEATURE exists\""
                ))
                .build()
        ))
        .build();
    Utils.assertYamlEquals("workflow/variables.yml", pipeline);
  }
}
