package io.gitlab.gitlabcidkjava.model.pipeline.job;

import java.io.IOException;
import java.util.Arrays;

import org.junit.Test;

import io.gitlab.gitlabcidkjava.model.pipeline.Pipeline;
import io.gitlab.gitlabcidkjava.model.pipeline.job.image.Image;
import io.gitlab.gitlabcidkjava.model.pipeline.job.image.PullPolicy;
import io.gitlab.gitlabcidkjava.model.pipeline.test.Utils;

public class ImageTests {
  @Test
  public void nameTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job")
                .image(Image.builder()
                    .name("registry.example.com/my/image:latest")
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/image/name.yml", pipeline);
  }

  @Test
  public void entrypointTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job")
                .image(Image.builder()
                    .name("super/sql:experimental")
                    .entrypoint(Arrays.asList(""))
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/image/entrypoint.yml", pipeline);
  }

  @Test
  public void pullPolicyTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("job1")
                .script(Arrays.asList("echo \"A single pull policy.\""))
                .image(Image.builder()
                    .name("ruby:3.0")
                    .pullPolicies(Arrays.asList(PullPolicy.IF_NOT_PRESENT))
                    .build())
                .build(),
            Job.builder()
                .id("job2")
                .script(Arrays.asList("echo \"Multiple pull policies.\""))
                .image(Image.builder()
                    .name("ruby:3.0")
                    .pullPolicies(Arrays.asList(PullPolicy.ALWAYS, PullPolicy.IF_NOT_PRESENT))
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/image/pull_policy.yml", pipeline);
  }
}
