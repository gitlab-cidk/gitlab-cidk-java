package io.gitlab.gitlabcidkjava.model.pipeline.job;

import io.gitlab.gitlabcidkjava.model.pipeline.Pipeline;
import io.gitlab.gitlabcidkjava.model.pipeline.job.environment.DeploymentTier;
import io.gitlab.gitlabcidkjava.model.pipeline.job.environment.Environment;
import io.gitlab.gitlabcidkjava.model.pipeline.job.environment.EnvironmentAction;
import io.gitlab.gitlabcidkjava.model.pipeline.test.Utils;

import java.io.IOException;
import java.util.Arrays;
import org.junit.Test;

public class EnvironmentTests {
  @Test
  public void nameTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("deploy to production")
                .stage("deploy")
                .script(Arrays.asList("git push production HEAD:main"))
                .environment(Environment.builder()
                    .name("production")
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/environment/name.yml", pipeline);
  }

  @Test
  public void urlTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("deploy to production")
                .stage("deploy")
                .script(Arrays.asList("git push production HEAD:main"))
                .environment(Environment.builder()
                    .name("production")
                    .url("https://prod.example.com")
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/environment/url.yml", pipeline);
  }

  @Test
  public void onStopTest() throws IOException {
    Job stopJob = Job.builder()
        .id("stop")
        .script(Arrays.asList("echo \"An example script section.\""))
        .environment(Environment.builder()
            .name("production")
            .action(EnvironmentAction.STOP)
            .build())
        .build();

    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            stopJob,
            Job.builder()
                .id("deploy to production")
                .stage("deploy")
                .script(Arrays.asList("git push production HEAD:main"))
                .environment(Environment.builder()
                    .name("production")
                    .onStop(stopJob)
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/environment/on_stop.yml", pipeline);
  }

  @Test
  public void actionTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("stop_review_app")
                .stage("deploy")
                .variables(Arrays.asList(
                    Variable.builder()
                        .key("GIT_STRATEGY")
                        .value("none")
                        .build()
                ))
                .script(Arrays.asList("make delete-app"))
                .when(JobWhen.MANUAL)
                .environment(Environment.builder()
                    .name("review/$CI_COMMIT_REF_SLUG")
                    .action(EnvironmentAction.STOP)
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/environment/action.yml", pipeline);
  }

  @Test
  public void autoStopInTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("review_app")
                .script(Arrays.asList("deploy-review-app"))
                .environment(Environment.builder()
                    .name("review/$CI_COMMIT_REF_SLUG")
                    .autoStopIn("1 day")
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/environment/auto_stop_in.yml", pipeline);
  }

  @Test
  public void deploymentTierTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("deploy")
                .script(Arrays.asList("echo"))
                .environment(Environment.builder()
                    .name("customer-portal")
                    .deploymentTier(DeploymentTier.PRODUCTION)
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/environment/deployment_tier.yml", pipeline);
  }

  @Test
  public void dynamicTest() throws IOException {
    Pipeline pipeline = Pipeline.builder()
        .jobs(Arrays.asList(
            Job.builder()
                .id("deploy as review app")
                .stage("deploy")
                .script(Arrays.asList("make deploy"))
                .environment(Environment.builder()
                    .name("review/$CI_COMMIT_REF_SLUG")
                    .url("https://$CI_ENVIRONMENT_SLUG.example.com/")
                    .build())
                .build()
        ))
        .build();
    Utils.assertYamlEquals("job/environment/dynamic.yml", pipeline);
  }
}
