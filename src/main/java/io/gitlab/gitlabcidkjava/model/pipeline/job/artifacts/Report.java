package io.gitlab.gitlabcidkjava.model.pipeline.job.artifacts;

import java.util.Map;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#artifactsreports">.gitlab-ci.yml keyword
 * reference</a>.
 */
@Getter
@SuperBuilder(toBuilder = true)
public abstract class Report {
  protected final String type;

  protected Report(@NonNull final String type) {
    this.type = type;
  }

  public abstract void writeToYamlDto(final Map<String, Object> yamlDto);
}
