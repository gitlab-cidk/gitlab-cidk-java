package io.gitlab.gitlabcidkjava.model.pipeline.job.idtokens;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#id_tokens">.gitlab-ci.yml keyword reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class IdToken {
  private final String id;
  private final List<String> audiences;

  public IdToken(@NonNull String id, @NonNull List<String> audiences) {
    this.id = id;
    this.audiences = Collections.unmodifiableList(audiences);
  }

  public void writeToYamlDto(final Map<String, Object> yamlDto) {
    HashMap<String, Object> idTokenDto = new HashMap<>();
    idTokenDto.put("aud", new ArrayList<>(audiences));
    yamlDto.put(id, idTokenDto);
  }
}
