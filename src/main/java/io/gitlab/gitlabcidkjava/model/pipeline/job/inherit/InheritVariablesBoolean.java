package io.gitlab.gitlabcidkjava.model.pipeline.job.inherit;

import java.util.Map;
import lombok.Builder;
import lombok.Getter;

/**
 * A boolean form of `inherit:variables`.
 */
@Getter
@Builder(toBuilder = true)
public class InheritVariablesBoolean extends InheritVariables {
  private boolean value;

  public InheritVariablesBoolean(boolean value) {
    this.value = value;
  }

  @Override
  public void writeToYamlDto(Map<String, Object> yamlDto) {
    yamlDto.put("variables", value);
  }
}
