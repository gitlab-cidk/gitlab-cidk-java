package io.gitlab.gitlabcidkjava.model.pipeline.job.inherit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

/**
 * A list form of `inherit:default`.
 */
@Getter
@Builder(toBuilder = true)
public class InheritDefaultList extends InheritDefault {
  private final List<String> values;

  public InheritDefaultList(@NonNull List<String> values) {
    this.values = Collections.unmodifiableList(values);
  }

  @Override
  public void writeToYamlDto(Map<String, Object> yamlDto) {
    yamlDto.put("default", new ArrayList<>(values));
  }
}
