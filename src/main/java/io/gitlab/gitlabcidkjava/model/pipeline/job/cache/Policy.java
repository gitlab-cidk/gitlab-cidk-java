package io.gitlab.gitlabcidkjava.model.pipeline.job.cache;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#cachepolicy">.gitlab-ci.yml keyword
 * reference</a>.
 */
public enum Policy {
  PULL("pull"),
  PUSH("push"),
  PULL_PUSH("pull-push");

  private final String yamlString;

  Policy(String yamlString) {
    this.yamlString = yamlString;
  }

  public String toYamlString() {
    return yamlString;
  }
}
