package io.gitlab.gitlabcidkjava.model.pipeline.job.cache;

import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

/**
 * The "default" form of `cache:key`.
 */
@Getter
@Builder(toBuilder = true)
public class CacheDefinitionKeyString extends CacheDefinitionKey {
  private final String value;

  public CacheDefinitionKeyString(@NonNull String value) {
    this.value = value;
  }

  @Override
  public void writeToYamlDto(Map<String, Object> yamlDto) {
    yamlDto.put("key", value);
  }
}
