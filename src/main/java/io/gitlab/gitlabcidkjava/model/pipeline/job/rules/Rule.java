package io.gitlab.gitlabcidkjava.model.pipeline.job.rules;

import io.gitlab.gitlabcidkjava.model.Utils;
import io.gitlab.gitlabcidkjava.model.pipeline.job.JobWhen;
import io.gitlab.gitlabcidkjava.model.pipeline.job.needs.Needs;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#rules">.gitlab-ci.yml keyword reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class Rule {
  private final String ifExpression;
  private final Changes changes;
  private final List<String> exists;
  private final Boolean allowFailure;
  private final JobWhen when;
  private final Map<String, String> variables;
  private final Needs needs;

  private Rule(String ifExpression, Changes changes, List<String> exists, Boolean allowFailure,
               JobWhen when, Map<String, String> variables, Needs needs) {
    this.ifExpression = ifExpression;
    this.changes = changes;
    this.exists = Utils.unmodifiableListOrNull(exists);
    this.allowFailure = allowFailure;
    this.when = when;
    this.variables = Utils.unmodifiableMapOrNull(variables);
    this.needs = needs;
  }

  public void writeToYamlDto(Map<String, Object> yamlDto) {
    if (ifExpression != null) {
      yamlDto.put("if", ifExpression);
    }
    if (changes != null) {
      changes.writeToYamlDto(yamlDto);
    }
    if (exists != null) {
      yamlDto.put("exists", new ArrayList<>(exists));
    }
    if (allowFailure != null) {
      yamlDto.put("allow_failure", allowFailure);
    }
    if (when != null) {
      yamlDto.put("when", when.toYamlString());
    }
    if (variables != null) {
      yamlDto.put("variables", new HashMap<>(variables));
    }
    if (needs != null) {
      needs.writeToYamlDto(yamlDto);
    }
  }
}
