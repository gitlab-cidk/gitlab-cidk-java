package io.gitlab.gitlabcidkjava.model.pipeline.workflow;

import io.gitlab.gitlabcidkjava.model.Utils;
import io.gitlab.gitlabcidkjava.model.pipeline.job.rules.Changes;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#workflowrules">.gitlab-ci.yml keyword
 * reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class WorkflowRule {
  private final String ifExpression;
  private final Changes changes;
  private final List<String> exists;
  private final WorkflowWhen when;
  private final Map<String, String> variables;

  private WorkflowRule(String ifExpression, Changes changes, List<String> exists, WorkflowWhen when,
                       Map<String, String> variables) {
    this.ifExpression = ifExpression;
    this.changes = changes;
    this.exists = Utils.unmodifiableListOrNull(exists);
    this.when = when;
    this.variables = Utils.unmodifiableMapOrNull(variables);
  }

  public void writeToYamlDto(Map<String, Object> yamlDto) {
    if (ifExpression != null) {
      yamlDto.put("if", ifExpression);
    }
    if (changes != null) {
      changes.writeToYamlDto(yamlDto);
    }
    if (exists != null) {
      yamlDto.put("exists", new ArrayList<>(exists));
    }
    if (when != null) {
      yamlDto.put("when", when.toYamlString());
    }
    if (variables != null) {
      yamlDto.put("variables", new HashMap<>(variables));
    }
  }
}
