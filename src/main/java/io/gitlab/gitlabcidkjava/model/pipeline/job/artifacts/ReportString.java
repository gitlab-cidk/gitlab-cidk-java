package io.gitlab.gitlabcidkjava.model.pipeline.job.artifacts;

import java.util.Map;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

/**
 * A string form of `artifacts:reports`.
 */
@Getter
@SuperBuilder(toBuilder = true)
public class ReportString extends Report {
  private final String configuration;

  public ReportString(@NonNull final String type, @NonNull final String configuration) {
    super(type);
    this.configuration = configuration;
  }

  @Override
  public void writeToYamlDto(Map<String, Object> yamlDto) {
    yamlDto.put(type, configuration);
  }
}
