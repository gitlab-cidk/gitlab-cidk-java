package io.gitlab.gitlabcidkjava.model.pipeline.job.trigger;

import java.util.HashMap;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#trigger">.gitlab-ci.yml keyword reference</a>.
 */
@Getter
@Builder
public class Trigger {
  private final String include;
  private final String project;
  private final String branch;
  private final Strategy strategy;
  private final Forward forward;

  private Trigger(String include, String project, String branch, Strategy strategy,
                  Forward forward) {
    // TODO: a bunch of validations
    this.include = include;
    this.project = project;
    this.branch = branch;
    this.strategy = strategy;
    this.forward = forward;
  }

  public void writeToYamlDto(Map<String, Object> yamlDto) {
    final Map<String, Object> triggerDto = new HashMap<>();
    if (include != null) {
      triggerDto.put("include", include);
    }
    if (project != null) {
      triggerDto.put("project", project);
    }
    if (branch != null) {
      triggerDto.put("branch", branch);
    }
    if (strategy != null) {
      triggerDto.put("strategy", strategy.toYamlString());
    }
    if (forward != null) {
      forward.writeToYamlDto(triggerDto);
    }
    yamlDto.put("trigger", triggerDto);
  }
}
