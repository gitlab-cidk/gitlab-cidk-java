package io.gitlab.gitlabcidkjava.model.pipeline.job.rules;

import io.gitlab.gitlabcidkjava.model.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#ruleschanges">.gitlab-ci.yml keyword
 * reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class Changes {
  private final List<String> paths;
  private final String compareTo;

  private Changes(List<String> paths, String compareTo) {
    this.paths = Utils.unmodifiableListOrNull(paths);
    this.compareTo = compareTo;
  }

  public void writeToYamlDto(Map<String, Object> yamlDto) {
    final Map<String, Object> changes = new HashMap<>();
    if (paths != null) {
      changes.put("paths", new ArrayList<>(paths));
    }
    if (compareTo != null) {
      changes.put("compare_to", compareTo);
    }
    yamlDto.put("changes", changes);
  }
}
