package io.gitlab.gitlabcidkjava.model.pipeline.job.allowfailure;

import java.util.Map;
import lombok.Builder;
import lombok.Getter;

/**
 * The "default" form of `allow_failure`.
 */
@Getter
@Builder(toBuilder = true)
public class AllowFailureBoolean extends AllowFailure {
  private final boolean allowFailure;

  public AllowFailureBoolean(boolean allowFailure) {
    this.allowFailure = allowFailure;
  }

  @Override
  public void writeToYamlDto(Map<String, Object> yamlDto) {
    yamlDto.put("allow_failure", allowFailure);
  }
}
