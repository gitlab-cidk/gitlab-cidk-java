package io.gitlab.gitlabcidkjava.model.pipeline.job.secrets;

import java.util.HashMap;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#secretsazure_key_vault">.gitlab-ci.yml keyword
 * reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class AzureKeyVault {
  private final String name;
  private final String version;

  private AzureKeyVault(String name, String version) {
    this.name = name;
    this.version = version;
  }

  public void writeToYamlDto(Map<String, Object> yamlDto) {
    final Map<String, Object> vaultDto = new HashMap<>();
    if (name != null) {
      vaultDto.put("name", name);
    }
    if (version != null) {
      vaultDto.put("version", version);
    }
    yamlDto.put("azure_key_vault", vaultDto);
  }
}
