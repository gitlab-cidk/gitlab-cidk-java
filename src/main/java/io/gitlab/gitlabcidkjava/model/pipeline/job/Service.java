package io.gitlab.gitlabcidkjava.model.pipeline.job;

import io.gitlab.gitlabcidkjava.model.Utils;
import io.gitlab.gitlabcidkjava.model.pipeline.job.image.PullPolicy;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#services">.gitlab-ci.yml keyword reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class Service {
  private final String name;
  private final String alias;
  private final List<String> entrypoint;
  private final List<String> command;
  private final List<PullPolicy> pullPolicies;

  private Service(@NonNull String name, String alias, List<String> entrypoint, List<String> command,
                  List<PullPolicy> pullPolicies) {
    this.name = name;
    this.alias = alias;
    this.entrypoint = Utils.unmodifiableListOrNull(entrypoint);
    this.command = Utils.unmodifiableListOrNull(command);
    this.pullPolicies = pullPolicies;
  }

  public void writeToYamlDto(final Map<String, Object> yamlDto) {
    yamlDto.put("name", name);
    if (alias != null) {
      yamlDto.put("alias", alias);
    }
    if (entrypoint != null) {
      yamlDto.put("entrypoint", new ArrayList<>(entrypoint));
    }
    if (command != null) {
      yamlDto.put("command", new ArrayList<>(command));
    }
    if (pullPolicies != null) {
      final List<Object> pullPolicyList = new ArrayList<>();
      for (PullPolicy pullPolicy : pullPolicies) {
        pullPolicyList.add(pullPolicy.toYamlString());
      }
      yamlDto.put("pull_policy", pullPolicyList);
    }
  }
}
