package io.gitlab.gitlabcidkjava.model.pipeline.job.image;

import io.gitlab.gitlabcidkjava.model.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#image">.gitlab-ci.yml keyword reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class Image {
  private final String name;
  private final List<String> entrypoint;
  private final List<PullPolicy> pullPolicies;

  private Image(@NonNull String name, List<String> entrypoint, List<PullPolicy> pullPolicies) {
    this.name = name;
    this.entrypoint = Utils.unmodifiableListOrNull(entrypoint);
    this.pullPolicies = Utils.unmodifiableListOrNull(pullPolicies);
  }

  public void writeToYamlDto(Map<String, Object> yamlDto) {
    final Map<String, Object> imageDto = new HashMap<>();
    imageDto.put("name", name);
    if (entrypoint != null) {
      imageDto.put("entrypoint", new ArrayList<>(entrypoint));
    }
    if (pullPolicies != null) {
      List<String> pullPoliciesStrings = pullPolicies.stream()
          .map(PullPolicy::toYamlString)
          .collect(Collectors.toList());
      imageDto.put("pull_policy", new ArrayList<>(pullPoliciesStrings));
    }
    yamlDto.put("image", imageDto);
  }
}
