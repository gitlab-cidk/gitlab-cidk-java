package io.gitlab.gitlabcidkjava.model.pipeline.job.environment;

import io.gitlab.gitlabcidkjava.model.pipeline.job.Job;
import java.util.HashMap;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#environment">.gitlab-ci.yml keyword
 * reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class Environment {
  private final String name;
  private final String url;
  private final Job onStop;
  private final EnvironmentAction action;
  private final String autoStopIn;
  private final DeploymentTier deploymentTier;

  private Environment(@NonNull final String name, final String url, final Job onStop,
                      final EnvironmentAction action, final String autoStopIn,
                      final DeploymentTier deploymentTier) {
    this.name = name;
    this.url = url;
    this.onStop = onStop;
    this.action = action;
    this.autoStopIn = autoStopIn;
    this.deploymentTier = deploymentTier;
  }

  public void writeToYamlDto(Map<String, Object> yamlDto) {
    final Map<String, Object> environmentDto = new HashMap<>();
    environmentDto.put("name", name);
    if (url != null) {
      environmentDto.put("url", url);
    }
    if (onStop != null) {
      environmentDto.put("on_stop", onStop.getId());
    }
    if (action != null) {
      environmentDto.put("action", action.toYamlString());
    }
    if (autoStopIn != null) {
      environmentDto.put("auto_stop_in", autoStopIn);
    }
    if (deploymentTier != null) {
      environmentDto.put("deployment_tier", deploymentTier.toYamlString());
    }
    yamlDto.put("environment", environmentDto);
  }
}
