package io.gitlab.gitlabcidkjava.model.pipeline.job.artifacts;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

/**
 * A map form of `artifacts:reports`.
 */
@Getter
@SuperBuilder(toBuilder = true)
public class ReportMap extends Report {
  private final Map<String, String> configuration;

  public ReportMap(@NonNull final String type, @NonNull final Map<String, String> configuration) {
    super(type);
    this.configuration = Collections.unmodifiableMap(configuration);
  }

  @Override
  public void writeToYamlDto(Map<String, Object> yamlDto) {
    yamlDto.put(type, new HashMap<>(configuration));
  }
}
