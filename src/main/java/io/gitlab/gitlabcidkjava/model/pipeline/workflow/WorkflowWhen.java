package io.gitlab.gitlabcidkjava.model.pipeline.workflow;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#workflowrules">.gitlab-ci.yml keyword
 * reference</a>.
 */
public enum WorkflowWhen {
  ALWAYS("always"),
  NEVER("never");

  private final String yamlString;

  WorkflowWhen(String yamlString) {
    this.yamlString = yamlString;
  }

  public String toYamlString() {
    return yamlString;
  }
}
