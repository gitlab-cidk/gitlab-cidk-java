package io.gitlab.gitlabcidkjava.model.pipeline.job.inherit;

import java.util.Map;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#inheritdefault">.gitlab-ci.yml keyword
 * reference</a>.
 */
public abstract class InheritDefault {
  public abstract void writeToYamlDto(final Map<String, Object> yamlDto);
}
