package io.gitlab.gitlabcidkjava.model.pipeline.job;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#when">.gitlab-ci.yml keyword reference</a>.
 */
public enum JobWhen {
  ON_SUCCESS("on_success"),
  MANUAL("manual"),
  ALWAYS("always"),
  ON_FAILURE("on_failure"),
  DELAYED("delayed"),
  NEVER("never");

  private final String yamlString;

  JobWhen(String yamlString) {
    this.yamlString = yamlString;
  }

  public String toYamlString() {
    return yamlString;
  }
}
