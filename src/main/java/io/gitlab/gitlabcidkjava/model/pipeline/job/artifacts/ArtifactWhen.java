package io.gitlab.gitlabcidkjava.model.pipeline.job.artifacts;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#artifactswhen">.gitlab-ci.yml keyword
 * reference</a>.
 */
public enum ArtifactWhen {
  ON_SUCCESS("on_success"),
  ON_FAILURE("on_failure"),
  ALWAYS("always");

  private final String yamlString;

  ArtifactWhen(String yamlString) {
    this.yamlString = yamlString;
  }

  public String toYamlString() {
    return yamlString;
  }
}
