package io.gitlab.gitlabcidkjava.model.pipeline;

import io.gitlab.gitlabcidkjava.model.Utils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#include">.gitlab-ci.yml keyword reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class PipelineInclude {
  private final String local;
  private final String project;
  private final List<String> file;
  private final String ref;
  private final String remote;
  private final String template;

  private PipelineInclude(String local, String project, List<String> file, String ref,
                          String remote, String template) {
    // TODO: lots of validation?
    this.local = local;
    this.project = project;
    this.file = Utils.unmodifiableListOrNull(file);
    this.ref = ref;
    this.remote = remote;
    this.template = template;
  }

  public void writeToYamlDto(Map<String, Object> yamlDto) {
    if (this.local != null) {
      yamlDto.put("local", this.local);
    }
    if (this.project != null) {
      yamlDto.put("project", this.project);
    }
    if (this.file != null) {
      yamlDto.put("file", new ArrayList<>(file));
    }
    if (this.ref != null) {
      yamlDto.put("ref", this.ref);
    }
    if (this.remote != null) {
      yamlDto.put("remote", this.remote);
    }
    if (this.template != null) {
      yamlDto.put("template", this.template);
    }
  }
}
