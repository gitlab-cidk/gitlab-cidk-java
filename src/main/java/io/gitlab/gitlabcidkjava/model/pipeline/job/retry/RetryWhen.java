package io.gitlab.gitlabcidkjava.model.pipeline.job.retry;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#retrywhen">.gitlab-ci.yml keyword reference</a>.
 */
public enum RetryWhen {
  ALWAYS("always"),
  UNKNOWN_FAILURE("unknown_failure"),
  SCRIPT_FAILURE("script_failure"),
  API_FAILURE("api_failure"),
  STUCK_OR_TIMEOUT_FAILURE("stuck_or_timeout_failure"),
  RUNNER_SYSTEM_FAILURE("runner_system_failure"),
  RUNNER_UNSUPPORTED("runner_unsupported"),
  STALE_SCHEDULE("stale_schedule"),
  JOB_EXECUTION_TIMEOUT("job_execution_timeout"),
  ARCHIVED_FAILURE("archived_failure"),
  UNMET_PREREQUISITES("unmet_prerequisites"),
  SCHEDULER_FAILURE("scheduler_failure"),
  DATA_INTEGRITY_FAILURE("data_integrity_failure");

  private final String yamlString;

  RetryWhen(String yamlString) {
    this.yamlString = yamlString;
  }

  public String toYamlString() {
    return yamlString;
  }
}
