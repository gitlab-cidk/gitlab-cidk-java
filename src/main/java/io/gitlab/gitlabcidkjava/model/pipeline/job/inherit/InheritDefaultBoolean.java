package io.gitlab.gitlabcidkjava.model.pipeline.job.inherit;

import java.util.Map;
import lombok.Builder;
import lombok.Getter;

/**
 * A boolean form of `inherit:default`.
 */
@Getter
@Builder(toBuilder = true)
public class InheritDefaultBoolean extends InheritDefault {
  private boolean value;

  public InheritDefaultBoolean(boolean value) {
    this.value = value;
  }

  @Override
  public void writeToYamlDto(Map<String, Object> yamlDto) {
    yamlDto.put("default", value);
  }
}
