package io.gitlab.gitlabcidkjava.model.pipeline.job.release;

import io.gitlab.gitlabcidkjava.model.Utils;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#release">.gitlab-ci.yml keyword reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class Release {
  private final String tagName;
  private final String tagMessage;
  private final String name;
  private final String description;
  private final String ref;
  private final List<String> milestones;
  private final ZonedDateTime releasedAt;
  private final List<AssetLink> assetsLinks;

  private Release(@NonNull String tagName, String tagMessage, String name, String description,
                  String ref, List<String> milestones, ZonedDateTime releasedAt,
                  List<AssetLink> assetsLinks) {
    this.tagName = tagName;
    this.tagMessage = tagMessage;
    this.name = name;
    this.description = description;
    this.ref = ref;
    this.milestones = Utils.unmodifiableListOrNull(milestones);
    this.releasedAt = releasedAt;
    this.assetsLinks = Utils.unmodifiableListOrNull(assetsLinks);
  }

  public void writeToYamlDto(Map<String, Object> yamlDto) {
    final Map<String, Object> releaseDto = new HashMap<>();
    releaseDto.put("tag_name", tagName);
    if (tagMessage != null) {
      releaseDto.put("tag_message", tagMessage);
    }
    if (name != null) {
      releaseDto.put("name", name);
    }
    if (description != null) {
      releaseDto.put("description", description);
    }
    if (ref != null) {
      releaseDto.put("ref", ref);
    }
    if (milestones != null) {
      releaseDto.put("milestones", milestones);
    }
    if (releasedAt != null) {
      releaseDto.put("released_at", releasedAt.toString());
    }
    if (assetsLinks != null) {
      final List<Map<String, Object>> linksDto = new ArrayList<>();
      for (AssetLink assetLink : assetsLinks) {
        final Map<String, Object> assetLinkDto = new HashMap<>();
        assetLink.writeToYamlDto(assetLinkDto);
        linksDto.add(assetLinkDto);
      }
      final Map<String, Object> assetsDto = new HashMap<>();
      assetsDto.put("links", linksDto);
      releaseDto.put("assets", assetsDto);
    }
    yamlDto.put("release", releaseDto);
  }
}
