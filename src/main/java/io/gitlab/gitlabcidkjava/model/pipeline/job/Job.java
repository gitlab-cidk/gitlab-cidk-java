package io.gitlab.gitlabcidkjava.model.pipeline.job;

import io.gitlab.gitlabcidkjava.model.Utils;
import io.gitlab.gitlabcidkjava.model.pipeline.job.allowfailure.AllowFailure;
import io.gitlab.gitlabcidkjava.model.pipeline.job.artifacts.Artifacts;
import io.gitlab.gitlabcidkjava.model.pipeline.job.cache.Cache;
import io.gitlab.gitlabcidkjava.model.pipeline.job.environment.Environment;
import io.gitlab.gitlabcidkjava.model.pipeline.job.idtokens.IdTokens;
import io.gitlab.gitlabcidkjava.model.pipeline.job.image.Image;
import io.gitlab.gitlabcidkjava.model.pipeline.job.inherit.Inherit;
import io.gitlab.gitlabcidkjava.model.pipeline.job.needs.Needs;
import io.gitlab.gitlabcidkjava.model.pipeline.job.parallel.Parallel;
import io.gitlab.gitlabcidkjava.model.pipeline.job.release.Release;
import io.gitlab.gitlabcidkjava.model.pipeline.job.retry.Retry;
import io.gitlab.gitlabcidkjava.model.pipeline.job.rules.Rule;
import io.gitlab.gitlabcidkjava.model.pipeline.job.secrets.Secret;
import io.gitlab.gitlabcidkjava.model.pipeline.job.trigger.Trigger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#job-keywords">.gitlab-ci.yml keyword
 * reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class Job {
  private final String id;
  private final List<String> afterScript;
  private final AllowFailure allowFailure;
  private final Artifacts artifacts;
  private final List<String> beforeScript;
  private final Cache cache;
  private final String coverage;
  private final DastConfiguration dastConfiguration;
  private final List<Job> dependencies;
  private final Environment environment;
  private final JobExtends jobExtends;
  private final Hooks hooks;
  private final IdTokens idTokens;
  private final Image image;
  private final Inherit inherit;
  private final Boolean interruptible;
  private final Needs needs;
  private final Parallel parallel;
  private final Release release;
  private final String resourceGroup;
  private final Retry retry;
  private final List<Rule> rules;
  private final List<String> script;
  private final List<Secret> secrets;
  private final List<Service> services;
  private final String stage;
  private final List<String> tags;
  private final String timeout;
  private final Trigger trigger;
  private final List<Variable> variables;
  private final JobWhen when;

  private Job(@NonNull final String id, final List<String> afterScript,
              final AllowFailure allowFailure, final Artifacts artifacts,
              final List<String> beforeScript, final Cache cache, final String coverage,
              final DastConfiguration dastConfiguration, final List<Job> dependencies,
              final Environment environment, final JobExtends jobExtends, final Hooks hooks,
              final IdTokens idTokens, final Image image, final Inherit inherit,
              final Boolean interruptible, final Needs needs, final Parallel parallel,
              final Release release, final String resourceGroup, final Retry retry,
              final List<Rule> rules, final List<String> script, final List<Secret> secrets,
              final List<Service> services, final String stage, final List<String> tags,
              final String timeout, final Trigger trigger, final List<Variable> variables,
              final JobWhen when) {
    this.id = id;
    this.afterScript = Utils.unmodifiableListOrNull(afterScript);
    this.allowFailure = allowFailure;
    this.artifacts = artifacts;
    this.beforeScript = Utils.unmodifiableListOrNull(beforeScript);
    this.cache = cache;
    this.coverage = coverage;
    this.dastConfiguration = dastConfiguration;
    this.dependencies = Utils.unmodifiableListOrNull(dependencies);
    this.environment = environment;
    this.jobExtends = jobExtends;
    this.hooks = hooks;
    this.idTokens = idTokens;
    this.image = image;
    this.inherit = inherit;
    this.interruptible = interruptible;
    this.needs = needs;
    this.parallel = parallel;
    this.release = release;
    this.resourceGroup = resourceGroup;
    this.retry = retry;
    this.rules = Utils.unmodifiableListOrNull(rules);
    this.script = Utils.unmodifiableListOrNull(script);
    this.secrets = Utils.unmodifiableListOrNull(secrets);
    this.services = Utils.unmodifiableListOrNull(services);
    this.stage = stage;
    this.tags = Utils.unmodifiableListOrNull(tags);
    this.timeout = timeout;
    this.trigger = trigger;
    this.variables = Utils.unmodifiableListOrNull(variables);
    this.when = when;
  }

  public void writeToYamlDto(final Map<String, Object> yamlDto) {
    final Map<String, Object> jobDto = new HashMap<>();
    if (afterScript != null) {
      jobDto.put("after_script", new ArrayList<>(afterScript));
    }
    if (allowFailure != null) {
      allowFailure.writeToYamlDto(jobDto);
    }
    if (artifacts != null) {
      artifacts.writeToYamlDto(jobDto);
    }
    if (beforeScript != null) {
      jobDto.put("before_script", new ArrayList<>(beforeScript));
    }
    if (cache != null) {
      cache.writeToYamlDto(jobDto);
    }
    if (coverage != null) {
      jobDto.put("coverage", coverage);
    }
    if (dastConfiguration != null) {
      dastConfiguration.writeToYamlDto(jobDto);
    }
    if (dependencies != null) {
      List<String> dependencyIds = new ArrayList<>();
      for (Job dependency : dependencies) {
        dependencyIds.add(dependency.getId());
      }
      jobDto.put("dependencies", dependencyIds);
    }
    if (environment != null) {
      environment.writeToYamlDto(jobDto);
    }
    if (jobExtends != null) {
      jobExtends.writeToYamlDto(jobDto);
    }
    if (hooks != null) {
      hooks.writeToYamlDto(jobDto);
    }
    if (idTokens != null) {
      idTokens.writeToYamlDto(jobDto);
    }
    if (image != null) {
      image.writeToYamlDto(jobDto);
    }
    if (inherit != null) {
      inherit.writeToYamlDto(jobDto);
    }
    if (interruptible != null) {
      jobDto.put("interruptible", interruptible);
    }
    if (needs != null) {
      needs.writeToYamlDto(jobDto);
    }
    if (parallel != null) {
      parallel.writeToYamlDto(jobDto);
    }
    if (release != null) {
      release.writeToYamlDto(jobDto);
    }
    if (resourceGroup != null) {
      jobDto.put("resource_group", resourceGroup);
    }
    if (retry != null) {
      retry.writeToYamlDto(jobDto);
    }
    if (rules != null) {
      List<Map<String, Object>> rulesDto = new ArrayList<>();
      for (Rule rule : rules) {
        Map<String, Object> ruleDto = new HashMap<>();
        rule.writeToYamlDto(ruleDto);
        rulesDto.add(ruleDto);
      }
      jobDto.put("rules", rulesDto);
    }
    if (script != null) {
      jobDto.put("script", new ArrayList<>(script));
    }
    if (secrets != null) {
      Map<String, Object> secretsDto = new HashMap<>();
      for (Secret secret : secrets) {
        secret.writeToYamlDto(secretsDto);
      }
      jobDto.put("secrets", secretsDto);
    }
    if (services != null) {
      List<Map<String, Object>> servicesDto = new ArrayList<>();
      for (Service service : services) {
        Map<String, Object> serviceDto = new HashMap<>();
        service.writeToYamlDto(serviceDto);
        servicesDto.add(serviceDto);
      }
      jobDto.put("services", servicesDto);
    }
    if (stage != null) {
      jobDto.put("stage", stage);
    }
    if (tags != null) {
      jobDto.put("tags", new ArrayList<>(tags));
    }
    if (timeout != null) {
      jobDto.put("timeout", timeout);
    }
    if (trigger != null) {
      trigger.writeToYamlDto(jobDto);
    }
    if (variables != null) {
      Map<String, Object> variablesDto = new HashMap<>();
      for (Variable variable : variables) {
        variable.writeToYamlDto(variablesDto);
      }
      jobDto.put("variables", variablesDto);
    }
    if (when != null) {
      jobDto.put("when", when.toYamlString());
    }
    yamlDto.put(id, jobDto);
  }
}
