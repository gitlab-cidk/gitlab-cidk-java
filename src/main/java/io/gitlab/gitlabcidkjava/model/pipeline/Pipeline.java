package io.gitlab.gitlabcidkjava.model.pipeline;

import io.gitlab.gitlabcidkjava.model.Utils;
import io.gitlab.gitlabcidkjava.model.pipeline.job.Job;
import io.gitlab.gitlabcidkjava.model.pipeline.job.Variable;
import io.gitlab.gitlabcidkjava.model.pipeline.workflow.Workflow;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/">.gitlab-ci.yml keyword reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class Pipeline {
  private final PipelineDefault pipelineDefault;
  private final List<PipelineInclude> include;
  private final List<String> stages;
  private final Workflow workflow;
  private final List<Job> jobs;
  private final List<Variable> variables;

  private Pipeline(PipelineDefault pipelineDefault, List<PipelineInclude> include,
                   List<String> stages, Workflow workflow, List<Job> jobs,
                   List<Variable> variables) {
    // TODO: validate that the stages set in the jobs are correct
    // TODO: validate that job ids are unique
    this.pipelineDefault = pipelineDefault;
    this.include = Utils.unmodifiableListOrNull(include);
    this.stages = Utils.unmodifiableListOrNull(stages);
    this.workflow = workflow;
    this.jobs = Utils.unmodifiableListOrNull(jobs);
    this.variables = variables;
  }

  public Map<String, Object> toYamlDto() {
    final Map<String, Object> yamlDto = new HashMap<>();
    if (this.pipelineDefault != null) {
      this.pipelineDefault.writeToYamlDto(yamlDto);
    }
    if (this.include != null) {
      final List<Map<String, Object>> includesDto = new ArrayList<>();
      for (final PipelineInclude include : this.include) {
        final Map<String, Object> includeDto = new HashMap<>();
        include.writeToYamlDto(includeDto);
        includesDto.add(includeDto);
      }
      yamlDto.put("include", includesDto);
    }
    if (this.workflow != null) {
      this.workflow.writeToYamlDto(yamlDto);
    }
    if (this.stages != null) {
      yamlDto.put("stages", new ArrayList<>(this.stages));
    }
    if (this.jobs != null) {
      for (Job job : this.jobs) {
        job.writeToYamlDto(yamlDto);
      }
    }
    if (variables != null) {
      Map<String, Object> variablesDto = new HashMap<>();
      for (Variable variable : variables) {
        variable.writeToYamlDto(variablesDto);
      }
      yamlDto.put("variables", variablesDto);
    }
    return yamlDto;
  }
}
