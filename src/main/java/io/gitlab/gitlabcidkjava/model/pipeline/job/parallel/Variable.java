package io.gitlab.gitlabcidkjava.model.pipeline.job.parallel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

/**
 * A variable used in `parallel:matrix`.
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#parallelmatrix">.gitlab-ci.yml keyword
 * reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class Variable {
  private final String key;
  private final List<String> values;

  public Variable(@NonNull String key, @NonNull List<String> values) {
    this.key = key;
    this.values = Collections.unmodifiableList(values);
  }

  public void writeToYamlDto(Map<String, Object> yamlDto) {
    yamlDto.put(key, new ArrayList<>(values));
  }
}
