package io.gitlab.gitlabcidkjava.model.pipeline.job.needs;

import java.util.Map;
import lombok.Builder;
import lombok.Getter;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#needs">.gitlab-ci.yml keyword reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class Need {
  private final String job;
  private final Boolean artifacts;
  private final String project;
  private final String ref;
  private final String pipeline;
  private final Boolean optional;

  private Need(String job, Boolean artifacts, String project, String ref, String pipeline,
               Boolean optional) {
    this.job = job;
    this.artifacts = artifacts;
    this.project = project;
    this.ref = ref;
    this.pipeline = pipeline;
    this.optional = optional;
  }

  public void writeToYamlDto(Map<String, Object> yamlDto) {
    if (job != null) {
      yamlDto.put("job", job);
    }
    if (artifacts != null) {
      yamlDto.put("artifacts", artifacts);
    }
    if (project != null) {
      yamlDto.put("project", project);
    }
    if (ref != null) {
      yamlDto.put("ref", ref);
    }
    if (pipeline != null) {
      yamlDto.put("pipeline", pipeline);
    }
    if (optional != null) {
      yamlDto.put("optional", optional);
    }
  }
}
