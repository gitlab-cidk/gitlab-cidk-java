package io.gitlab.gitlabcidkjava.model.pipeline.job.cache;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#cache">.gitlab-ci.yml keyword reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class Cache {
  private final List<CacheDefinition> caches;

  public Cache(@NonNull List<CacheDefinition> caches) {
    this.caches = Collections.unmodifiableList(caches);
  }

  public void writeToYamlDto(final Map<String, Object> yamlDto) {
    List<Map<String, Object>> cacheDtos = new ArrayList<>();
    for (CacheDefinition cache : caches) {
      HashMap<String, Object> cacheDto = new HashMap<>();
      cache.writeToYamlDto(cacheDto);
      cacheDtos.add(cacheDto);
    }
    yamlDto.put("cache", cacheDtos);
  }
}
