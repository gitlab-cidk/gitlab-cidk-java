package io.gitlab.gitlabcidkjava.model.pipeline.job.environment;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#environmentdeployment_tier">.gitlab-ci.yml
 * keyword reference</a>.
 */
public enum DeploymentTier {
  PRODUCTION("production"),

  STAGING("staging"),

  TESTING("testing"),

  DEVELOPMENT("development"),

  OTHER("other");

  private final String yamlString;

  DeploymentTier(String yamlString) {
    this.yamlString = yamlString;
  }

  public String toYamlString() {
    return yamlString;
  }
}
