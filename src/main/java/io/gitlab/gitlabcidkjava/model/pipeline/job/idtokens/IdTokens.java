package io.gitlab.gitlabcidkjava.model.pipeline.job.idtokens;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#id_tokens">.gitlab-ci.yml keyword reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class IdTokens {
  private final List<IdToken> idTokens;

  public IdTokens(@NonNull List<IdToken> idTokens) {
    this.idTokens = Collections.unmodifiableList(idTokens);
  }

  public void writeToYamlDto(final Map<String, Object> yamlDto) {
    final HashMap<String, Object> idTokensDto = new HashMap<>();
    for (IdToken idToken : idTokens) {
      idToken.writeToYamlDto(idTokensDto);
    }
    yamlDto.put("id_tokens", idTokensDto);
  }
}
