package io.gitlab.gitlabcidkjava.model.pipeline.job.trigger;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#triggerstrategy">.gitlab-ci.yml keyword
 * reference</a>.
 */
public enum Strategy {
  DEPEND("depend");

  private final String yamlString;

  Strategy(String yamlString) {
    this.yamlString = yamlString;
  }

  public String toYamlString() {
    return yamlString;
  }
}
