package io.gitlab.gitlabcidkjava.model.pipeline.workflow;

import io.gitlab.gitlabcidkjava.model.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#workflow">.gitlab-ci.yml keyword reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class Workflow {
  private final String name;
  private final List<WorkflowRule> rules;

  private Workflow(String name, List<WorkflowRule> rules) {
    this.name = name;
    this.rules = Utils.unmodifiableListOrNull(rules);
  }

  public void writeToYamlDto(Map<String, Object> yamlDto) {
    final Map<String, Object> workflowDto = new HashMap<>();
    if (name != null) {
      workflowDto.put("name", name);
    }
    if (rules != null) {
      final List<Map<String, Object>> rulesDto = new ArrayList<>();
      for (WorkflowRule rule : rules) {
        final HashMap<String, Object> ruleDto = new HashMap<>();
        rule.writeToYamlDto(ruleDto);
        rulesDto.add(ruleDto);
      }
      workflowDto.put("rules", rulesDto);
    }
    yamlDto.put("workflow", workflowDto);
  }
}
