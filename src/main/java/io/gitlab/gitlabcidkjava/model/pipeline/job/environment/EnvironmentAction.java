package io.gitlab.gitlabcidkjava.model.pipeline.job.environment;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#environmentaction">.gitlab-ci.yml keyword
 * reference</a>.
 */
public enum EnvironmentAction {
  START("start"),
  PREPARE("prepare"),
  STOP("stop"),
  VERIFY("verify"),
  ACCESS("access");

  private final String yamlString;

  EnvironmentAction(String yamlString) {
    this.yamlString = yamlString;
  }

  public String toYamlString() {
    return yamlString;
  }
}
