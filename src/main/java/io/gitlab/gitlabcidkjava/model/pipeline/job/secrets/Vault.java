package io.gitlab.gitlabcidkjava.model.pipeline.job.secrets;

import java.util.HashMap;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#secretsvault">.gitlab-ci.yml keyword
 * reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class Vault {
  private final String engineName;
  private final String enginePath;
  private final String path;
  private final String field;

  private Vault(String engineName, String enginePath, String path, String field) {
    this.engineName = engineName;
    this.enginePath = enginePath;
    this.path = path;
    this.field = field;
  }

  public void writeToYamlDto(Map<String, Object> yamlDto) {
    final Map<String, Object> vaultDto = new HashMap<>();
    if (engineName != null || enginePath != null) {
      final Map<String, Object> engineDto = new HashMap<>();
      if (engineName != null) {
        engineDto.put("name", engineName);
      }
      if (enginePath != null) {
        engineDto.put("path", enginePath);
      }
      vaultDto.put("engine", engineDto);
    }
    if (path != null) {
      vaultDto.put("path", path);
    }
    if (field != null) {
      vaultDto.put("field", field);
    }
    yamlDto.put("vault", vaultDto);
  }
}
