package io.gitlab.gitlabcidkjava.model.pipeline.job.retry;

import io.gitlab.gitlabcidkjava.model.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#retry">.gitlab-ci.yml keyword reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class Retry {
  private final int max;
  private final List<RetryWhen> when;

  private Retry(int max, List<RetryWhen> when) {
    // TODO: max can only be 0, 1 or 2
    this.max = max;
    this.when = Utils.unmodifiableListOrNull(when);
  }

  public void writeToYamlDto(Map<String, Object> yamlDto) {
    if (when != null) {
      final Map<String, Object> retryDto = new HashMap<>();
      retryDto.put("max", max);
      final List<Object> whenDto = new ArrayList<>();
      for (final RetryWhen when : when) {
        whenDto.add(when.toYamlString());
      }
      retryDto.put("when", whenDto);
      yamlDto.put("retry", retryDto);
    } else {
      yamlDto.put("retry", max);
    }
  }
}
