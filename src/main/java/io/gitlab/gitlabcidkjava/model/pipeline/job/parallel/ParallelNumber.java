package io.gitlab.gitlabcidkjava.model.pipeline.job.parallel;

import java.util.Map;
import lombok.Builder;
import lombok.Getter;

/**
 * A "default" form of `parallel`.
 */
@Getter
@Builder(toBuilder = true)
public class ParallelNumber extends Parallel {
  private final int number;

  public ParallelNumber(int number) {
    this.number = number;
  }

  @Override
  public void writeToYamlDto(Map<String, Object> yamlDto) {
    yamlDto.put("parallel", number);
  }
}
