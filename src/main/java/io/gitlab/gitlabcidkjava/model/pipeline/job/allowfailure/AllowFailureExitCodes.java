package io.gitlab.gitlabcidkjava.model.pipeline.job.allowfailure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

/**
 * The extended form of `allow_failure`.
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#allow_failureexit_codes">.gitlab-ci.yml keyword
 * reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class AllowFailureExitCodes extends AllowFailure {
  private final List<Integer> exitCodes;

  public AllowFailureExitCodes(@NonNull List<Integer> exitCodes) {
    this.exitCodes = Collections.unmodifiableList(exitCodes);
  }

  @Override
  public void writeToYamlDto(Map<String, Object> yamlDto) {
    final HashMap<String, Object> allowFailureDto = new HashMap<>();
    allowFailureDto.put("exit_codes", new ArrayList<>(exitCodes));
    yamlDto.put("allow_failure", allowFailureDto);
  }
}
