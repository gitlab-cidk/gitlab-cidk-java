package io.gitlab.gitlabcidkjava.model.pipeline.job.inherit;

import java.util.Map;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#inheritvariables">.gitlab-ci.yml keyword
 * reference</a>.
 */
public abstract class InheritVariables {
  public abstract void writeToYamlDto(final Map<String, Object> yamlDto);
}
