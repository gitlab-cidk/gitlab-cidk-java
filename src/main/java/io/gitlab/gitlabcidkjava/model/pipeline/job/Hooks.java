package io.gitlab.gitlabcidkjava.model.pipeline.job;

import io.gitlab.gitlabcidkjava.model.Utils;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#hooks">.gitlab-ci.yml keyword reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class Hooks {
  private final List<String> preGetSourcesScript;

  private Hooks(List<String> preGetSourcesScript) {
    this.preGetSourcesScript = Utils.unmodifiableListOrNull(preGetSourcesScript);
  }

  public void writeToYamlDto(final Map<String, Object> yamlDto) {
    Map<String, Object> hooksDto = new HashMap<>();
    if (preGetSourcesScript != null) {
      hooksDto.put("pre_get_sources_script", preGetSourcesScript);
    }
    yamlDto.put("hooks", hooksDto);
  }
}
