package io.gitlab.gitlabcidkjava.model.pipeline.job.cache;

import java.util.Map;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#cachekey">.gitlab-ci.yml keyword reference</a>.
 */
public abstract class CacheDefinitionKey {
  public abstract void writeToYamlDto(Map<String, Object> yamlDto);
}
