package io.gitlab.gitlabcidkjava.model.pipeline.job.parallel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#parallelmatrix">.gitlab-ci.yml keyword
 * reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class ParallelMatrix extends Parallel {
  private final List<List<Variable>> matrix;

  public ParallelMatrix(@NonNull List<List<Variable>> matrix) {
    this.matrix = Collections.unmodifiableList(matrix);
  }

  @Override
  public void writeToYamlDto(Map<String, Object> yamlDto) {
    final Map<String, Object> parallelDto = new HashMap<>();
    final List<Object> matrixDto = new ArrayList<>();
    for (List<Variable> entry : matrix) {
      final Map<String, Object> entryDto = new HashMap<>();
      for (Variable variable : entry) {
        variable.writeToYamlDto(entryDto);
      }
      matrixDto.add(entryDto);
    }
    parallelDto.put("matrix", matrixDto);
    yamlDto.put("parallel", parallelDto);
  }
}
