package io.gitlab.gitlabcidkjava.model.pipeline.job.release;

/**
 * Link type in `release:assets:links`.
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#releaseassetslinks">.gitlab-ci.yml keyword
 * reference</a>.
 */
public enum LinkType {
  RUNBOOK("runbook"),
  PACKAGE("package"),
  IMAGE("image"),
  OTHER("other");

  private final String yamlString;

  LinkType(String yamlString) {
    this.yamlString = yamlString;
  }

  public String toYamlString() {
    return yamlString;
  }
}
