package io.gitlab.gitlabcidkjava.model.pipeline.job;

import java.util.HashMap;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#dast_configuration">.gitlab-ci.yml keyword
 * reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class DastConfiguration {
  private final String siteProfile;
  private final String scannerProfile;

  private DastConfiguration(String siteProfile, String scannerProfile) {
    this.siteProfile = siteProfile;
    this.scannerProfile = scannerProfile;
  }

  public void writeToYamlDto(Map<String, Object> yamlDto) {
    final Map<String, Object> dastConfigurationDto = new HashMap<>();
    if (siteProfile != null) {
      dastConfigurationDto.put("site_profile", siteProfile);
    }
    if (scannerProfile != null) {
      dastConfigurationDto.put("scanner_profile", scannerProfile);
    }
    yamlDto.put("dast_configuration", dastConfigurationDto);
  }
}
