package io.gitlab.gitlabcidkjava.model.pipeline.job.cache;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#cachewhen">.gitlab-ci.yml keyword reference</a>.
 */
public enum CacheWhen {
  ON_SUCCESS("on_success"),
  ON_FAILURE("on_failure"),
  ALWAYS("always");

  private final String yamlString;

  CacheWhen(String yamlString) {
    this.yamlString = yamlString;
  }

  public String toYamlString() {
    return yamlString;
  }
}
