package io.gitlab.gitlabcidkjava.model.pipeline.job.cache;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#cachekeyfiles">cache:key:files</a> and <a
 * href="https://docs.gitlab.com/ee/ci/yaml/#cachekeyprefix">cache:key:prefix</a>.
 */
@Getter
@Builder(toBuilder = true)
public class CacheDefinitionKeyFilesPrefix extends CacheDefinitionKey {
  private final List<String> files;
  private final String prefix;

  private CacheDefinitionKeyFilesPrefix(@NonNull List<String> files, String prefix) {
    this.files = Collections.unmodifiableList(files);
    this.prefix = prefix;
  }

  @Override
  public void writeToYamlDto(Map<String, Object> yamlDto) {
    final Map<String, Object> keyDto = new HashMap<>();
    keyDto.put("files", new ArrayList<>(files));
    if (prefix != null) {
      keyDto.put("prefix", prefix);
    }
    yamlDto.put("key", keyDto);
  }
}
