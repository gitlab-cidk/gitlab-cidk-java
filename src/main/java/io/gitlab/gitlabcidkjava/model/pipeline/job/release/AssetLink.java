package io.gitlab.gitlabcidkjava.model.pipeline.job.release;

import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#releaseassetslinks">.gitlab-ci.yml keyword
 * reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class AssetLink {
  private final String name;
  private final String url;
  private final String filePath;
  private final LinkType linkType;

  private AssetLink(@NonNull String name, @NonNull String url, String filePath, LinkType linkType) {
    this.name = name;
    this.url = url;
    this.filePath = filePath;
    this.linkType = linkType;
  }

  public void writeToYamlDto(Map<String, Object> yamlDto) {
    yamlDto.put("name", name);
    yamlDto.put("url", url);
    if (filePath != null) {
      yamlDto.put("filepath", filePath);
    }
    if (linkType != null) {
      yamlDto.put("link_type", linkType.toYamlString());
    }
  }
}
