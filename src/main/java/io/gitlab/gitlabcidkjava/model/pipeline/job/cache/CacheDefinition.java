package io.gitlab.gitlabcidkjava.model.pipeline.job.cache;

import io.gitlab.gitlabcidkjava.model.Utils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#cache">.gitlab-ci.yml keyword reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class CacheDefinition {
  private final List<String> paths;
  private final CacheDefinitionKey key;
  private final Boolean untracked;
  private final Boolean unprotect;
  private final CacheWhen when;
  private final Policy policy;
  private final List<String> fallbackKeys;

  private CacheDefinition(List<String> paths, CacheDefinitionKey key, Boolean untracked,
                          Boolean unprotect, CacheWhen when, Policy policy,
                          List<String> fallbackKeys) {
    this.paths = Utils.unmodifiableListOrNull(paths);
    this.key = key;
    this.untracked = untracked;
    this.unprotect = unprotect;
    this.when = when;
    this.policy = policy;
    this.fallbackKeys = Utils.unmodifiableListOrNull(fallbackKeys);
  }

  public void writeToYamlDto(final Map<String, Object> yamlDto) {
    if (paths != null) {
      yamlDto.put("paths", new ArrayList<>(paths));
    }
    if (key != null) {
      key.writeToYamlDto(yamlDto);
    }
    if (untracked != null) {
      yamlDto.put("untracked", untracked);
    }
    if (unprotect != null) {
      yamlDto.put("unprotect", unprotect);
    }
    if (when != null) {
      yamlDto.put("when", when.toYamlString());
    }
    if (policy != null) {
      yamlDto.put("policy", policy.toYamlString());
    }
    if (fallbackKeys != null) {
      yamlDto.put("fallback_keys", fallbackKeys);
    }
  }
}
