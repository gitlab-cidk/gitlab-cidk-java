package io.gitlab.gitlabcidkjava.model.pipeline.job.inherit;

import java.util.HashMap;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#inherit">.gitlab-ci.yml keyword reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class Inherit {
  private final InheritDefault inheritDefault;
  private final InheritVariables inheritVariables;

  private Inherit(InheritDefault inheritDefault, InheritVariables inheritVariables) {
    this.inheritDefault = inheritDefault;
    this.inheritVariables = inheritVariables;
  }

  public void writeToYamlDto(final Map<String, Object> yamlDto) {
    final Map<String, Object> inheritDto = new HashMap<>();
    if (inheritDefault != null) {
      inheritDefault.writeToYamlDto(inheritDto);
    }
    if (inheritVariables != null) {
      inheritVariables.writeToYamlDto(inheritDto);
    }
    yamlDto.put("inherit", inheritDto);
  }
}
