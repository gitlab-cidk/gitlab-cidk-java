package io.gitlab.gitlabcidkjava.model.pipeline.job.needs;

import java.util.HashMap;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#needspipeline">.gitlab-ci.yml keyword
 * reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class NeedsPipeline extends Needs {
  private final String pipeline;

  public NeedsPipeline(@NonNull String pipeline) {
    this.pipeline = pipeline;
  }

  @Override
  public void writeToYamlDto(Map<String, Object> yamlDto) {
    final Map<String, Object> needsDto = new HashMap<>();
    needsDto.put("pipeline", pipeline);
    yamlDto.put("needs", needsDto);
  }
}
