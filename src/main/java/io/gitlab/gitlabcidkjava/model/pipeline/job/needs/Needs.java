package io.gitlab.gitlabcidkjava.model.pipeline.job.needs;

import java.util.Map;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#needs">.gitlab-ci.yml keyword reference</a>.
 */
public abstract class Needs {
  public abstract void writeToYamlDto(Map<String, Object> yamlDto);
}
