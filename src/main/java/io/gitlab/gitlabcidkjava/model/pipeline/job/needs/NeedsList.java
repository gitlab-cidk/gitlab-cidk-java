package io.gitlab.gitlabcidkjava.model.pipeline.job.needs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

/**
 * A "default" form of `needs`.
 */
@Getter
@Builder(toBuilder = true)
public class NeedsList extends Needs {
  private final List<Need> needs;

  public NeedsList(@NonNull List<Need> needs) {
    this.needs = Collections.unmodifiableList(needs);
  }

  public void writeToYamlDto(Map<String, Object> yamlDto) {
    final List<Map<String, Object>> needsDto = new ArrayList<>();
    for (Need need : needs) {
      final Map<String, Object> needDto = new HashMap<>();
      need.writeToYamlDto(needDto);
      needsDto.add(needDto);
    }
    yamlDto.put("needs", needsDto);
  }
}
