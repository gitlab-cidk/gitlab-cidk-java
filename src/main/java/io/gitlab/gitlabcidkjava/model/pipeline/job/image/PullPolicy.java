package io.gitlab.gitlabcidkjava.model.pipeline.job.image;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#imagepull_policy">.gitlab-ci.yml keyword
 * reference</a>.
 */
public enum PullPolicy {
  ALWAYS("always"),
  IF_NOT_PRESENT("if-not-present"),
  NEVER("never");

  private final String yamlString;

  PullPolicy(String yamlString) {
    this.yamlString = yamlString;
  }

  public String toYamlString() {
    return yamlString;
  }
}
