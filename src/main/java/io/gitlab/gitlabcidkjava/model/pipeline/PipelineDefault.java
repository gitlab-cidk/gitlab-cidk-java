package io.gitlab.gitlabcidkjava.model.pipeline;

import io.gitlab.gitlabcidkjava.model.Utils;
import io.gitlab.gitlabcidkjava.model.pipeline.job.Hooks;
import io.gitlab.gitlabcidkjava.model.pipeline.job.Service;
import io.gitlab.gitlabcidkjava.model.pipeline.job.artifacts.Artifacts;
import io.gitlab.gitlabcidkjava.model.pipeline.job.cache.Cache;
import io.gitlab.gitlabcidkjava.model.pipeline.job.image.Image;
import io.gitlab.gitlabcidkjava.model.pipeline.job.retry.Retry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#default">.gitlab-ci.yml keyword reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class PipelineDefault {
  private final List<String> afterScript;
  private final Artifacts artifacts;
  private final List<String> beforeScript;
  private final Cache cache;
  private final Hooks hooks;
  private final Image image;
  private final Boolean interruptible;
  private final Retry retry;
  private final List<Service> services;
  private final List<String> tags;
  private final String timeout;

  private PipelineDefault(List<String> afterScript, Artifacts artifacts, List<String> beforeScript,
                          Cache cache, Hooks hooks, Image image, Boolean interruptible, Retry retry,
                          List<Service> services, List<String> tags, String timeout) {
    this.afterScript = Utils.unmodifiableListOrNull(afterScript);
    this.artifacts = artifacts;
    this.beforeScript = Utils.unmodifiableListOrNull(beforeScript);
    this.cache = cache;
    this.hooks = hooks;
    this.image = image;
    this.interruptible = interruptible;
    this.retry = retry;
    this.services = Utils.unmodifiableListOrNull(services);
    this.tags = Utils.unmodifiableListOrNull(tags);
    this.timeout = timeout;
  }

  public void writeToYamlDto(Map<String, Object> yamlDto) {
    final Map<String, Object> defaultDto = new HashMap<>();
    if (afterScript != null) {
      defaultDto.put("after_script", new ArrayList<>(afterScript));
    }
    if (artifacts != null) {
      artifacts.writeToYamlDto(defaultDto);
    }
    if (beforeScript != null) {
      defaultDto.put("before_script", new ArrayList<>(beforeScript));
    }
    if (cache != null) {
      cache.writeToYamlDto(defaultDto);
    }
    if (hooks != null) {
      hooks.writeToYamlDto(defaultDto);
    }
    if (image != null) {
      image.writeToYamlDto(defaultDto);
    }
    if (interruptible != null) {
      defaultDto.put("interruptible", interruptible);
    }
    if (retry != null) {
      retry.writeToYamlDto(defaultDto);
    }
    if (services != null) {
      List<Map<String, Object>> servicesDto = new ArrayList<>();
      for (Service service : services) {
        Map<String, Object> serviceDto = new HashMap<>();
        service.writeToYamlDto(serviceDto);
        servicesDto.add(serviceDto);
      }
      defaultDto.put("services", servicesDto);
    }
    if (tags != null) {
      defaultDto.put("tags", new ArrayList<>(tags));
    }
    if (timeout != null) {
      defaultDto.put("timeout", timeout);
    }
    yamlDto.put("default", defaultDto);
  }
}
