package io.gitlab.gitlabcidkjava.model.pipeline.job.trigger;

import java.util.HashMap;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#triggerforward">.gitlab-ci.yml keyword
 * reference</a>.
 */
@Getter
@Builder
public class Forward {
  private final Boolean yamlVariables;
  private final Boolean pipelineVariables;

  private Forward(Boolean yamlVariables, Boolean pipelineVariables) {
    this.yamlVariables = yamlVariables;
    this.pipelineVariables = pipelineVariables;
  }

  public void writeToYamlDto(Map<String, Object> yamlDto) {
    final Map<String, Object> forwardDto = new HashMap<>();
    if (yamlVariables != null) {
      forwardDto.put("yaml_variables", yamlVariables);
    }
    if (pipelineVariables != null) {
      forwardDto.put("pipeline_variables", pipelineVariables);
    }
    yamlDto.put("forward", forwardDto);
  }
}
