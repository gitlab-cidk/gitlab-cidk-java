package io.gitlab.gitlabcidkjava.model.pipeline.job;

import io.gitlab.gitlabcidkjava.model.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#variables">.gitlab-ci.yml keyword reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class Variable {
  private final String key;
  private final String value;
  private final String description;
  private final List<String> options;
  private final Boolean expand;

  private Variable(@NonNull String key, String value, String description,
                   List<String> options, Boolean expand) {
    this.key = key;
    this.value = value;
    this.description = description;
    this.options = Utils.unmodifiableListOrNull(options);
    this.expand = expand;
  }

  public void writeToYamlDto(final Map<String, Object> yamlDto) {
    HashMap<String, Object> variableDto = new HashMap<>();
    if (value != null) {
      variableDto.put("value", value);
    }
    if (description != null) {
      variableDto.put("description", description);
    }
    if (options != null) {
      variableDto.put("options", new ArrayList<>(options));
    }
    if (expand != null) {
      variableDto.put("expand", expand);
    }
    yamlDto.put(key, variableDto);
  }
}
