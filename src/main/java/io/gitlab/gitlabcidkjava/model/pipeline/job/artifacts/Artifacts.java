package io.gitlab.gitlabcidkjava.model.pipeline.job.artifacts;

import io.gitlab.gitlabcidkjava.model.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#artifacts">.gitlab-ci.yml keyword reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class Artifacts {
  private final List<String> paths;
  private final List<String> exclude;
  private final String expireIn;
  private final String exposeAs;
  private final String name;
  private final Boolean isPublic;
  private final List<Report> reports;
  private final Boolean untracked;
  private final ArtifactWhen when;

  private Artifacts(final List<String> paths, final List<String> exclude, final String expireIn,
                    final String exposeAs, final String name, final Boolean isPublic,
                    final List<Report> reports, final Boolean untracked, final ArtifactWhen when) {
    this.paths = Utils.unmodifiableListOrNull(paths);
    this.exclude = Utils.unmodifiableListOrNull(exclude);
    this.expireIn = expireIn;
    this.exposeAs = exposeAs;
    this.name = name;
    this.isPublic = isPublic;
    this.reports = Utils.unmodifiableListOrNull(reports);
    this.untracked = untracked;
    this.when = when;
  }

  public void writeToYamlDto(final Map<String, Object> yamlDto) {
    final HashMap<String, Object> artifactsDto = new HashMap<>();
    if (paths != null) {
      artifactsDto.put("paths", new ArrayList<>(paths));
    }
    if (exclude != null) {
      artifactsDto.put("exclude", new ArrayList<>(exclude));
    }
    if (expireIn != null) {
      artifactsDto.put("expire_in", expireIn);
    }
    if (exposeAs != null) {
      artifactsDto.put("expose_as", exposeAs);
    }
    if (name != null) {
      artifactsDto.put("name", name);
    }
    if (isPublic != null) {
      artifactsDto.put("public", isPublic);
    }
    if (reports != null) {
      final Map<String, Object> reportsDto = new HashMap<>();
      for (final Report report : reports) {
        report.writeToYamlDto(reportsDto);
      }
      artifactsDto.put("reports", reportsDto);
    }
    if (untracked != null) {
      artifactsDto.put("untracked", untracked);
    }
    if (when != null) {
      artifactsDto.put("when", when.toYamlString());
    }
    yamlDto.put("artifacts", artifactsDto);
  }
}
