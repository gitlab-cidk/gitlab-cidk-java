package io.gitlab.gitlabcidkjava.model.pipeline.job.artifacts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

/**
 * A list form of `artifacts:reports`.
 */
@Getter
@SuperBuilder(toBuilder = true)
public class ReportList extends Report {
  private final List<String> configuration;

  public ReportList(@NonNull final String type, @NonNull final List<String> configuration) {
    super(type);
    this.configuration = Collections.unmodifiableList(configuration);
  }

  @Override
  public void writeToYamlDto(Map<String, Object> yamlDto) {
    yamlDto.put(type, new ArrayList<>(configuration));
  }
}
