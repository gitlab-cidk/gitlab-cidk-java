package io.gitlab.gitlabcidkjava.model.pipeline.job.secrets;

import java.util.HashMap;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#secrets">.gitlab-ci.yml keyword reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class Secret {
  private final String id;
  private final Vault vault;
  private final Boolean file;
  private final String token;
  private final AzureKeyVault azureKeyVault;

  private Secret(@NonNull String id, Vault vault, Boolean file, String token,
                 AzureKeyVault azureKeyVault) {
    this.id = id;
    this.vault = vault;
    this.file = file;
    this.token = token;
    this.azureKeyVault = azureKeyVault;
  }

  public void writeToYamlDto(Map<String, Object> yamlDto) {
    final Map<String, Object> secretDto = new HashMap<>();
    if (vault != null) {
      vault.writeToYamlDto(secretDto);
    }
    if (file != null) {
      secretDto.put("file", file);
    }
    if (token != null) {
      secretDto.put("token", token);
    }
    if (azureKeyVault != null) {
      azureKeyVault.writeToYamlDto(secretDto);
    }
    yamlDto.put(id, secretDto);
  }
}
