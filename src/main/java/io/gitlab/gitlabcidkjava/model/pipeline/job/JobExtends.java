package io.gitlab.gitlabcidkjava.model.pipeline.job;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#extends">.gitlab-ci.yml keyword reference</a>.
 */
@Getter
@Builder(toBuilder = true)
public class JobExtends {
  private final List<Job> jobs;

  public JobExtends(@NonNull List<Job> jobs) {
    this.jobs = Collections.unmodifiableList(jobs);
  }

  public void writeToYamlDto(final Map<String, Object> yamlDto) {
    List<String> jobIds = new ArrayList<>();
    for (Job job : jobs) {
      jobIds.add(job.getId());
    }
    yamlDto.put("extends", jobIds);
  }
}
