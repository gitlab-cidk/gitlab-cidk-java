package io.gitlab.gitlabcidkjava.model.pipeline.job.allowfailure;

import java.util.Map;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#allow_failure">.gitlab-ci.yml keyword
 * reference</a>.
 */
public abstract class AllowFailure {
  public abstract void writeToYamlDto(final Map<String, Object> yamlDto);
}
