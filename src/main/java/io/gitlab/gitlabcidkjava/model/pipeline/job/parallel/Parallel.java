package io.gitlab.gitlabcidkjava.model.pipeline.job.parallel;

import java.util.Map;

/**
 * See <a href="https://docs.gitlab.com/ee/ci/yaml/#parallel">.gitlab-ci.yml keyword reference</a>.
 */
public abstract class Parallel {
  public abstract void writeToYamlDto(Map<String, Object> yamlDto);
}
