package io.gitlab.gitlabcidkjava.model;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Miscellaneous utilities for internal use.
 */
public class Utils {
  public static <T> List<T> unmodifiableListOrNull(List<T> list) {
    return list != null ? Collections.unmodifiableList(list) : null;
  }

  public static <K, V> Map<K, V> unmodifiableMapOrNull(Map<K, V> map) {
    return map != null ? Collections.unmodifiableMap(map) : null;
  }
}
