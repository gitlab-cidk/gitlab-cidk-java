package io.gitlab.gitlabcidkjava.renderer;

import com.esotericsoftware.yamlbeans.YamlException;
import com.esotericsoftware.yamlbeans.YamlWriter;
import io.gitlab.gitlabcidkjava.model.pipeline.Pipeline;
import java.io.StringWriter;
import java.io.Writer;

/**
 * Responsible for rendering a pipeline to YAML.
 */
public class PipelineRenderer {
  public String toString(final Pipeline pipeline) {
    final StringWriter stringWriter = new StringWriter();
    final YamlWriter yamlWriter = new YamlWriter(stringWriter);
    try {
      yamlWriter.write(pipeline.toYamlDto());
      yamlWriter.close();
    } catch (YamlException e) {
      throw new PipelineRendererException(e);
    }
    return stringWriter.toString();
  }

  public void toWriter(final Pipeline pipeline, final Writer writer) {
    final YamlWriter yamlWriter = new YamlWriter(writer);
    try {
      yamlWriter.write(pipeline.toYamlDto());
      yamlWriter.close();
    } catch (YamlException e) {
      throw new PipelineRendererException(e);
    }
  }
}
