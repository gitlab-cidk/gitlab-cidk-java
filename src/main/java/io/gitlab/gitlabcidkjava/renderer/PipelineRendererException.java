package io.gitlab.gitlabcidkjava.renderer;

/**
 * Thrown when a pipeline rendering error occurs.
 */
public class PipelineRendererException extends RuntimeException {
  public PipelineRendererException(String message, Throwable cause) {
    super(message, cause);
  }

  public PipelineRendererException(String message) {
    super(message);
  }

  public PipelineRendererException(Throwable cause) {
    super(cause);
  }
}
