$commit_msg = "chore: Update CHANGELOG.md"
docker run --rm -v ${PWD}:/data orhunp/git-cliff:sha-9692ea7 -r /data --with-commit "$commit_msg" -o /data/CHANGELOG.md
git add CHANGELOG.md
git commit -m "$commit_msg"
