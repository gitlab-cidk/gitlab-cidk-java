<img src="design/logo-256.png" height="128" />

<!--
![Liberapay](https://img.shields.io/liberapay/patrons/kdunee)
[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/gitlab-cidk/gitlab-cidk-java)
![Version](https://img.shields.io/maven-central/v/io.gitlab.gitlab-cidk/gitlab-cidk-java)
![Coverage](https://img.shields.io/gitlab/pipeline-coverage/gitlab-cidk/gitlab-cidk-java)
-->
[![Source code](https://img.shields.io/badge/repository-GitLab-green)](https://gitlab.com/gitlab-cidk/gitlab-cidk-java)
[![Documentation](https://img.shields.io/badge/documentation-gitbook-green)](https://gitlab-cidk.gitlab.io)
[![License](https://img.shields.io/gitlab/license/gitlab-cidk/gitlab-cidk-java)](LICENSE)
[![Issues](https://img.shields.io/gitlab/issues/open/gitlab-cidk/gitlab-cidk-java)](https://gitlab.com/gitlab-cidk/gitlab-cidk-java/-/issues)
[![Build](https://img.shields.io/gitlab/pipeline-status/gitlab-cidk/gitlab-cidk-java)](https://gitlab.com/gitlab-cidk/gitlab-cidk-java/-/pipelines?scope=branches&ref=main)
[![Discord](https://img.shields.io/discord/1109377469691211819)](https://discord.gg/m2FXbnGhEm)

**GitLab CIDK Java** is a Java library designed to simplify the process of defining and generating GitLab CI/CD pipelines programmatically, allowing for seamless integration with your development workflow.

> Please note that **GitLab CIDK Java** is an independent open source project and is not affiliated with or supported by GitLab Inc. or its subsidiaries.

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Usage](#usage)
- [Compatibility](#compatibility)
- [Contributing](#contributing)
- [License](#license)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Usage

Visit [USER_GUIDE.md](USER_GUIDE.md) for information on using **GitLab CIDK Java**.

## Compatibility

- **GitLab CIDK Java** is compatible with JVM in version 8 and higher. This includes any JVM-compatible languages, such as Java, Scala or Kotlin.
- **GitLab CIDK Java** may lack some of the deprecated features of GitLab CI/CD.
- Wherever multiple notations for the same definition are possible in GitLab CI/CD, **GitLab CIDK Java** may offer only one of them.

## Contributing
Visit [CONTRIBUTING.md](CONTRIBUTING.md) for information on building **GitLab CIDK Java** from source or contributing improvements.

## License
**GitLab CIDK Java** is licensed under the MIT License. See [LICENSE](LICENSE) for more details.
