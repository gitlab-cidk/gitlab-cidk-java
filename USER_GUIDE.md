# User Guide

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Installation](#installation)
  - [Apache Maven](#apache-maven)
  - [Gradle](#gradle)
- [Usage](#usage)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Installation

### Apache Maven

    <dependency>
      <groupId>io.gitlab.gitlab-cidk</groupId>
      <artifactId>gitlab-cidk-java</artifactId>
      <version>1.0.0</version>
    </dependency>

### Gradle

    dependencies {
      implementation 'io.gitlab.gitlab-cidk:gitlab-cidk-java:1.0.0'
    }

## Usage

For usage example in Kotlin, see the `pipeline` directory.
