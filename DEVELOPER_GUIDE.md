# Developer Guide

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Development environment](#development-environment)
  - [Maven wrapper](#maven-wrapper)
  - [Code formatting](#code-formatting)
- [Commit guidelines](#commit-guidelines)
- [Testing and static analysis](#testing-and-static-analysis)
  - [Editorconfig & Code formatting](#editorconfig--code-formatting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Development environment

### Maven wrapper

This repository uses [Maven Wrapper](https://maven.apache.org/wrapper/). This means that you only need Java (8+) installed on your machine for development.

You can use the wrapper script (`mvnw` or `mvnw.cmd`, depending on the OS) to run Apache Maven.

### Code formatting

The `tools/formatter.xml` file contains Eclipse-style code formatting definition. This file can be imported into your IDE of choice (Eclipse, IntelliJ IDEA etc.).

This definition should be mostly compatible with the configuration of the `maven-checkstyle-plugin`, used for code formatting validation.

The formatter is also available as a Maven plugin, for validation:

``` shell
mvn formatter:validate
```

Or enforcing the style:

``` shell
mvn formatter:format
```

## Commit guidelines

This repository follows [Conventional Commits](https://www.conventionalcommits.org/). Please familiarize yourself with the convention before contributing.

## Testing and static analysis

Each code contribution should pass the automatic verification phase. This means running the `verify` Maven phase:

``` shell
mvn verify
```

This will execute several steps. In case of failures, their results can be inspected either directly in Maven output or in the reports generated in `target/site`.

### Editorconfig & Code formatting

Failures in the `editorconfig-maven-plugin` and `formatter-maven-plugin` can usually be easily fixed by running (respectively):

```shell
mvn editorconfig:format
```

or 

```shell
mvn formatter:format
```

to automatically enforce the rules.
